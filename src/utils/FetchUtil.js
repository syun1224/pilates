import PropTypes from 'prop-types';

const methodType = ['GET', 'POST', 'DELETE', 'UPDATE'];
const baseUrl = 'http://127.0.0.1:3000';

/* eslint-disable no-undef */
async function FetchUtil(url, method, body) {
  const request = {
    method: method && methodType.includes(method) ? method : 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  };
  if (body) request.body = body;
  const response = await fetch(`${baseUrl}/${url}`, request);
  if (response.ok) {
    const responseJson = await response.json();
    return responseJson;
  }
  const error = new Error(response.statusText);
  error.response = response;
  throw error;

  // .catch((err) => err);
}

export default FetchUtil;

FetchUtil.propTypes = {
  url: PropTypes.string,
  method: PropTypes.string,
  body: PropTypes.object,
};

FetchUtil.defaultProps = {
  url: '',
  method: 'GET',
  body: {},
};
