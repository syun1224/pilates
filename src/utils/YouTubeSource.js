
/* eslint-disable no-undef */
export default YTsource = (videoId, height) => ({
  html: `<html>
  <style type="text/css">    
      html, body {
          margin: 0;
          padding: 0;
          position: relative;
      }
  </style>
  
  <head>
      <meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width">
  </head>
  
  <body>
      <iframe id="YTPlayer" src="https://www.youtube-nocookie.com/embed/${videoId}" height="${height}" width="100%" allowfullscreen></iframe>
  </body>
  
  <script type="text/javascript">
      var tag = document.createElement('script');
      tag.src = "https://www.youtube.com/iframe_api";
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  
      var player;
      var timerId = 0;
      
      function onYouTubeIframeAPIReady() {
          player = new YT.Player('YTPlayer', {
            //   videoId: '${videoId}',
            //   height: ${height},                                                                                                                                                                                             
            //   width: '100%',
              events: {
                  'onStateChange': onPlayerStateChange,
                  'onError': onPlayerError,
              },
              playerVars: {
                  rel: 0,
                  disablekb: 1,
                 // origin: 'https://www.youtube.com'  not work!!!
              }
          });
      }
  
      function onPlayerStateChange(event) {
        if(event.data == YT.PlayerState.PLAYING){
            window.ReactNativeWebView.postMessage('PLAY');
        } else {
            window.ReactNativeWebView.postMessage('STOP');
        }
      }
  
      function onPlayerError(err) {
          window.ReactNativeWebView.postMessage(err);
      }
  
  </script>
  
  </html>`,
});
