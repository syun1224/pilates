export default function ColorFormatCheck(colorFormat) {
  if (
    /^(#)((?:[A-Fa-f0-9]{3}){1,2})$/.test(colorFormat)
  || /^(#)((?:[A-Fa-f0-9]{3})([0-9]))$/.test(colorFormat)
  || /^(rgb|hsl)(a?)[(]\s*([\d.]+\s*%?)\s*,\s*([\d.]+\s*%?)\s*,\s*([\d.]+\s*%?)\s*(?:,\s*([\d.]+)\s*)?[)]$/.test(colorFormat)
  ) return colorFormat;
  // eslint-disable-next-line no-console
  console.log('colorError!');
  return '#0000';
}
