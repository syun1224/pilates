import PropTypes from 'prop-types';
import DB from '../../mockData/db.json';

const methodType = ['GET', 'POST', 'DELETE', 'UPDATE'];

/* eslint-disable no-undef */
async function FetchUtil(url, method, body) {
  const {
    reservationDateList, groupClass, personalClass, filter,
  } = DB;
  try {
    const dataType = url.split('?');
    console.log('dataType', dataType);
    if (dataType[0] === 'reservationDateList') return reservationDateList;
    if (dataType[0] === 'personalClass') {
      const date = dataType[1].split('date=');
      const findObj = await personalClass.find((obj) => obj.date === date[1]);
      return [findObj];
    } if (dataType[0] === 'groupClass') {
      const date = dataType[1].split('date=');
      const findObj = await groupClass.find((obj) => obj.date === date[1]);
      return [findObj];
    } if (dataType[0] === 'filter') return filter;
    throw new Error('Not Found Data!');
  } catch (error) {
    return error;
  }
}

export default FetchUtil;

FetchUtil.propTypes = {
  url: PropTypes.string,
  method: PropTypes.string,
  body: PropTypes.object,
};

FetchUtil.defaultProps = {
  url: '',
  method: 'GET',
  body: {},
};
