import { createStore, applyMiddleware, compose } from 'redux';
import { createLogger } from 'redux-logger';
import createSagaMiddleware, { END } from 'redux-saga';
import rootReducer from './reducers';

const saga = createSagaMiddleware();

const configureStore = (preloadedState) => {
  const store = createStore(
    rootReducer,
    preloadedState,
    compose(
      applyMiddleware(saga, createLogger()),
    ),
  );
  store.runSaga = saga.run;
  store.close = () => store.dispatch(END);
  return store;
};

export default configureStore;
