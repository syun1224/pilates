import {
  call, spawn, put, takeEvery, take, delay,
} from 'redux-saga/effects';
import * as actions from '../actions';
// import fetch from '../../utils/FetchUtil';
import fetch from '../../utils/TestFetch';
import {
  GROUP_CLASS_REQUEST, PERSONAL_CLASS_REQUEST, RESERVATION_DATE_REQUEST, FILTER_REQUEST,
} from '../actionTypes';

function* fetchGroupClasses(date) {
  // try catch finally 구문으로 오류 제어가 가능하다.
  try {
    // 이부분을 call 메소드를 이용해 테스트가 쉽게 바꿀 수 있다.
    // (yeild를 사용하기 때문에 next 명령어로 반복 가능하므로)
    let dateStr;
    if (!date) yield put(actions.groupClassFail(new Error('Pram is Empty!')));
    else {
      if (typeof date === 'string') {
        dateStr = `?date=${date}`;
      } else if (typeof date === 'object') {
        date.forEach((element, i) => {
          if (i === 0) dateStr = `?date=${element}`;
          else dateStr += `&&date=${element}`;
        });
      }
      yield delay(1000);
      const data = yield call(fetch, `groupClass${dateStr || ''}`);
      yield put(actions.groupClassSuccess(data));
    }
  } catch (error) {
    yield put(actions.groupClassFail(error));
  }
}

function* fetchPersonalClasses(date) {
  // try catch finally 구문으로 오류 제어가 가능하다.
  try {
    // 이부분을 call 메소드를 이용해 테스트가 쉽게 바꿀 수 있다.
    // (yeild를 사용하기 때문에 next 명령어로 반복 가능하므로)
    let dateStr;
    if (!date) yield put(actions.personalClassFail(new Error('Param is Empty!')));
    else {
      if (typeof date === 'string') {
        dateStr = `?date=${date}`;
      } else if (typeof date === 'object') {
        date.forEach((element, i) => {
          if (i === 0) dateStr = `?date=${element}`;
          else dateStr += `&&date=${element}`;
        });
      }
      yield delay(1000);
      const data = yield call(fetch, `personalClass${dateStr || ''}`);
      yield put(actions.personalClassSuccess(data));
    }
  } catch (error) {
    yield put(actions.personalClassFail(error));
  }
}

function* fetchReservationDate() {
  try {
    const { dateList } = yield call(fetch, 'reservationDateList');
    yield put(actions.reservationDateSuccess(dateList));
  } catch (error) {
    yield put(actions.reservationDateFail(error));
  }
}

function* fetchFilter() {
  try {
    const data = yield call(fetch, 'filter');
    yield put(actions.filterSuccess(data));
  } catch (error) {
    yield put(actions.filterFail(error));
  }
}

function* watchClasses() {
  // type의 action이 실행되면 fetchBoardsSaga도 항상(Every) 실행한다
  yield takeEvery(GROUP_CLASS_REQUEST, ({ date, add }) => fetchGroupClasses(date, add));
  yield takeEvery(PERSONAL_CLASS_REQUEST, ({ date, add }) => fetchPersonalClasses(date, add));
  yield takeEvery(RESERVATION_DATE_REQUEST, fetchReservationDate);
  yield takeEvery(FILTER_REQUEST, fetchFilter);
}

export default function* root() {
  yield spawn(watchClasses);
}
