import { combineReducers } from 'redux';
import groupClass from './groupClass';
import personalClass from './personalClass';
import reservationDate from './reservationDate';
import filter from './filter';

const rootReducer = combineReducers({
  personalClass,
  groupClass,
  reservationDate,
  filter,
});

export default rootReducer;
