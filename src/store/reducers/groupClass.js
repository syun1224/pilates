import {
  GROUP_CLASS_REQUEST, GROUP_CLASS_SUCCESS,
  GROUP_CLASS_FAIL,
} from '../actionTypes';

const initialState = {
  isFetching: false,
  groupClasses: null,
  add: null,
  error: null,
};

const groupClass = (state = initialState, action) => {
  const {
    type, groupClasses = [], error,
  } = action;
  switch (type) {
    case GROUP_CLASS_REQUEST:
      return { ...state, isFetching: true };
    case GROUP_CLASS_SUCCESS: {
      return { ...state, groupClasses: groupClasses[0], isFetching: false };
    }
    case GROUP_CLASS_FAIL:
      return {
        ...state, error, isFetching: false,
      };
    default:
      return state;
  }
};

export default groupClass;
