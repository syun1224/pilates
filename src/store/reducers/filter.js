import {
  FILTER_REQUEST, FILTER_SUCCESS,
  FILTER_FAIL,
} from '../actionTypes';

const initialState = {
  isFetching: false,
  data: {},
  filterError: null,
};

const reservationDate = (state = initialState, action) => {
  const {
    type, data, error,
  } = action;
  switch (type) {
    case FILTER_REQUEST:
      return { ...state, isFetching: true };
    case FILTER_SUCCESS:
      return { ...state, data, isFetching: false };
    case FILTER_FAIL:
      return {
        ...state, error, isFetching: false,
      };
    default:
      return state;
  }
};

export default reservationDate;
