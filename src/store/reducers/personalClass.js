import {
  PERSONAL_CLASS_REQUEST, PERSONAL_CLASS_SUCCESS,
  PERSONAL_CLASS_FAIL,
} from '../actionTypes';

const initialState = {
  isFetching: false,
  personalClasses: null,
  add: null,
  error: null,
};

const personalClass = (state = initialState, action) => {
  const {
    type, personalClasses = [], error,
  } = action;
  switch (type) {
    case PERSONAL_CLASS_REQUEST:
      return { ...state, isFetching: true };
    case PERSONAL_CLASS_SUCCESS: {
      return { ...state, personalClasses: personalClasses[0], isFetching: false };
    }
    case PERSONAL_CLASS_FAIL:
      return {
        ...state, error, isFetching: false,
      };
    default:
      return state;
  }
};

export default personalClass;
