import {
  RESERVATION_DATE_REQUEST, RESERVATION_DATE_SUCCESS,
  RESERVATION_DATE_FAIL,
} from '../actionTypes';

const initialState = {
  isFetching: false,
  dateList: [],
  error: null,
};

const reservationDate = (state = initialState, action) => {
  const {
    type, dateList, error,
  } = action;
  switch (type) {
    case RESERVATION_DATE_REQUEST:
      return { ...state, isFetching: true };
    case RESERVATION_DATE_SUCCESS:
      return { ...state, dateList, isFetching: false };
    case RESERVATION_DATE_FAIL:
      return {
        ...state, error, isFetching: false,
      };
    default:
      return state;
  }
};

export default reservationDate;
