import {
  GROUP_CLASS_REQUEST, GROUP_CLASS_SUCCESS,
  GROUP_CLASS_FAIL,
  PERSONAL_CLASS_REQUEST, PERSONAL_CLASS_SUCCESS,
  PERSONAL_CLASS_FAIL,
  RESERVATION_DATE_REQUEST, RESERVATION_DATE_SUCCESS,
  RESERVATION_DATE_FAIL,
  FILTER_REQUEST, FILTER_SUCCESS,
  FILTER_FAIL,
} from '../actionTypes';

export const groupClassRequest = (date) => ({
  type: GROUP_CLASS_REQUEST,
  date,
});

// saga에서 호출하는 액션
export const groupClassSuccess = (groupClasses) => ({
  type: GROUP_CLASS_SUCCESS,
  groupClasses,
});

// saga에서 호출하는 액션
export const groupClassFail = (error) => ({
  type: GROUP_CLASS_FAIL,
  error,
});

export const personalClassRequest = (date) => ({
  type: PERSONAL_CLASS_REQUEST,
  date,
});

export const personalClassSuccess = (personalClasses) => ({
  type: PERSONAL_CLASS_SUCCESS,
  personalClasses,
});

export const personalClassFail = (error) => ({
  type: PERSONAL_CLASS_FAIL,
  error,
});

export const reservationDateRequest = () => ({
  type: RESERVATION_DATE_REQUEST,
});

export const reservationDateSuccess = (dateList) => ({
  type: RESERVATION_DATE_SUCCESS,
  dateList,
});


export const reservationDateFail = (error) => ({
  type: RESERVATION_DATE_FAIL,
  error,
});

export const filterRequest = () => ({
  type: FILTER_REQUEST,
});

export const filterSuccess = (data) => ({
  type: FILTER_SUCCESS,
  data,
});

export const filterFail = (error) => ({
  type: FILTER_FAIL,
  error,
});
