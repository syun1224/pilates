import React, { useEffect, useRef, useState } from 'react';
import {
  View, ScrollView, SafeAreaView, Image, TouchableOpacity, Animated, LayoutAnimation, Dimensions, TouchableWithoutFeedback, KeyboardAvoidingView, Platform,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import AntDesign from 'react-native-vector-icons/AntDesign';

import Header from '../../components/Header';
import { Text, Input } from '../../components/material';
import { REM } from '../../../constants/Config';

const { height } = Dimensions.get('window');
const color = '#faa';

const data = [
  {
    instructor: {
      name: '김혜인',
      image: 'https://scontent-iad3-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/c0.180.1440.1440a/s640x640/82563024_648714682604182_1001099521422807199_n.jpg?_nc_ht=scontent-iad3-1.cdninstagram.com&_nc_cat=110&_nc_ohc=xTai3hSS9GEAX9kyDva&oh=eacc100c9bc5da330e23cddfdd30ddcd&oe=5F007088',
    },
    classType: '그룹',
    branch: '잠실새내',
    classDate: '2020.02.10',
    classTime: '15:00 ~ 15:50',
    rating: { branch: 2, instructor: 3 },
  },
  {
    instructor: {
      name: '강다연',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTuajUXzSVg6u9Osypu9qxnQVQJeE-gioVNLEVj2aZ9ze4w3d9f&usqp=CAU',
    },
    classType: '개인',
    branch: '잠실새내',
    classDate: '2020.01.16',
    classTime: '13:00 ~ 13:50',
    rating: { branch: 4, instructor: 3 },
  },
  {
    instructor: {
      name: '김혜인',
      image: 'https://scontent-iad3-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/c0.180.1440.1440a/s640x640/82563024_648714682604182_1001099521422807199_n.jpg?_nc_ht=scontent-iad3-1.cdninstagram.com&_nc_cat=110&_nc_ohc=xTai3hSS9GEAX9kyDva&oh=eacc100c9bc5da330e23cddfdd30ddcd&oe=5F007088',
    },
    classType: '그룹',
    branch: '잠실새내',
    classDate: '2020.01.03',
    classTime: '09:00 ~ 09:50',
    rating: null,
  },
  {
    instructor: {
      name: '주인영',
      image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTeYS4Bzn1CCHD2mKMvVzsVIYHW-lsr0lXhj52bdmH8KNtNrXz0&usqp=CAU',
    },
    classType: '그룹',
    branch: '잠실새내',
    classDate: '2020.12.20',
    classTime: '15:00 ~ 15:50',
    rating: null,
  },
];

const styles = EStyleSheet.create({
  topLine: {
    width: '100%',
    borderBottomWidth: '1rem',
    borderBottomColor: '#9996',
  },
  surveyListWrap: {
    width: '100%',
  },
  surveyWrap: {
    width: '100%',
  },
  listWrap: {
    width: '100%',
    height: '70rem',
    alignSelf: 'center',
    borderBottomWidth: '1rem',
    borderBottomColor: '#9996',
    alignItems: 'center',
    flexDirection: 'row',
  },
  listTextWrap: {
    height: '40rem',
    justifyContent: 'space-between',
  },
  listEndWrap: {
    paddingRight: '20rem',
    flex: 1,
    alignItems: 'flex-end',
  },
  statusWrap: {
    width: '60rem',
    height: '50rem',
    borderRadius: '3rem',
    // backgroundColor: color,
    justifyContent: 'center',
    alignItems: 'center',
  },
  profileImg: {
    width: '56rem',
    height: '56rem',
    borderRadius: '28rem',
    marginHorizontal: '10rem',
  },
  buttonWrap: {
    width: '100%',
    height: '50rem',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: color,
  },
  cardShadow: {
    shadowOffset: { width: 0, height: 0.5 * 3 },
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * 3,
  },
  cardWrap: {
    height: '270rem',
    width: '350rem',
    alignItems: 'center',
    marginTop: '20rem',
    borderRadius: '20rem',
    backgroundColor: '#fff',
  },
  titleWrap: {
    paddingTop: '20rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
  starWrap: {
    padding: '5rem',
  },
});

export default function Survey({ navigation }) {
  const gradeInstructorArr = [1, 2, 3, 4, 5];
  const gradeBranchArr = [1, 2, 3, 4, 5];
  const [surveyIdx, setSurveyIdx] = useState(null);
  const [gradeInstructor, setGradeInstructor] = useState(0);
  const [gradeBranch, setGradeBranch] = useState(0);
  const [opinionInstructor, setOpinionInstructor] = useState('');
  const [opinionBranch, setOpinionBranch] = useState('');
  const scrollRef = useRef();
  const updateLayout = (index) => {
    if (surveyIdx !== null) {
      setSurveyIdx(null);
      setGradeInstructor(0);
      setGradeBranch(0);
      setOpinionInstructor('');
      setOpinionBranch('');
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    } else {
      setSurveyIdx(index);
      LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
      if (scrollRef.current) scrollRef.current.scrollTo({ x: 0, y: (20 * REM) + (index * 70 * REM) });
    }
  };
  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <Header navigation={navigation} color="#fff" goBack>만족도 조사</Header>
      <ScrollView ref={scrollRef} style={{ flex: 1 }} contentContainerStyle={{ alignItems: 'center', paddingVertical: 20 * REM }} scrollEnabled={surveyIdx === null} scrollToOverflowEnabled>
        <View style={styles.topLine} />
        {data.map((obj, i) => {
          const {
            instructor, branch, classDate, classTime, classType, rating,
          } = obj;
          return (
            <View style={styles.surveyListWrap}>
              <TouchableOpacity onPress={() => !rating && updateLayout(i)}>
                <View style={styles.listWrap}>
                  <Image source={{ uri: instructor.image }} style={styles.profileImg} />
                  <View style={styles.listTextWrap}>
                    <Text size={16} weight="bold"><Text size={18} color={color}>{`${classType} `}</Text>{`${instructor.name}(${branch})`}</Text>
                    <Text>{`${classDate} ${classTime}`}</Text>
                  </View>
                  <View style={styles.listEndWrap}>
                    <View style={styles.statusWrap}><Text color={color} size={15} weight="bold">{rating ? `지점:★${rating.branch}\n강사:★${rating.instructor}` : '평가하기'}</Text></View>
                  </View>
                </View>
              </TouchableOpacity>
              <View style={[styles.surveyWrap, { height: (surveyIdx === i) ? height - 250 * REM : 0 }]}>
                <KeyboardAwareScrollView style={{ flex: 1 }} contentContainerStyle={{ alignItems: 'center' }} extraHeight={200 * REM}>
                  <View style={styles.cardShadow}>
                    <View style={styles.cardWrap}>
                      <View style={styles.titleWrap}><Text color={color} size={20} weight="bold">수업 만족도</Text></View>
                      <View style={{ flexDirection: 'row', paddingTop: 10 * REM }}>
                        {gradeInstructorArr.map((grade) => (
                          <TouchableWithoutFeedback onPress={() => { setGradeInstructor(grade); }}>
                            <Animated.View style={styles.starWrap}>
                              {gradeInstructor >= grade
                                ? <AntDesign size={40 * REM} color={color} name="star" />
                                : <AntDesign size={40 * REM} color={color} name="staro" />}
                            </Animated.View>
                          </TouchableWithoutFeedback>
                        ))}
                      </View>
                      <View>
                        <Input
                          style={{ textAlignVertical: 'top' }}
                          placeholder="여러분의 소중한 의견이 좋은 수업을 만듭니다"
                          width={320}
                          height={110}
                          border="1 10 #5555"
                          multiline
                          onChangeText={(e) => setOpinionInstructor(e)}
                        />
                        <Text style={{ marginLeft: 5 * REM, marginTop: 5 * REM }} color="#5558">3점 이하 시 의견을 꼭 남겨주세요!</Text>
                      </View>
                    </View>
                  </View>
                  {/* <View style={{ height: 10* REM }} /> */}
                  <View style={styles.cardShadow}>
                    <View style={styles.cardWrap}>
                      <View style={styles.titleWrap}><Text color={color} size={20} weight="bold">지점 만족도</Text></View>
                      <View style={{ flexDirection: 'row', paddingTop: 10 * REM }}>
                        {gradeBranchArr.map((grade) => (
                          <TouchableWithoutFeedback onPress={() => { setGradeBranch(grade); }}>
                            <Animated.View style={styles.starWrap}>
                              {gradeBranch >= grade
                                ? <AntDesign size={40 * REM} color={color} name="star" />
                                : <AntDesign size={40 * REM} color={color} name="staro" />}
                            </Animated.View>
                          </TouchableWithoutFeedback>
                        ))}
                      </View>
                      <View>
                        <Input
                          style={{ textAlignVertical: 'top' }}
                          value={opinionBranch}
                          placeholder="여러분의 소중한 의견이 좋은 지점문화를 만듭니다"
                          width={320}
                          height={110}
                          border="1 10 #5555"
                          multiline
                          onChangeText={(e) => setOpinionBranch(e)}
                        />
                        <Text style={{ marginLeft: 5 * REM, marginTop: 5 * REM }} color="#5558">3점 이하 시 의견을 꼭 남겨주세요!</Text>
                      </View>
                    </View>
                  </View>
                  <View style={{ height: 20 * REM }} />
                </KeyboardAwareScrollView>
              </View>
            </View>
          );
        })}
        <SafeAreaView />
      </ScrollView>
      <View style={[styles.buttonWrap, { bottom: (surveyIdx !== null) ? 0 : -50 * REM }]}>
        <TouchableOpacity>
          <View style={[styles.buttonWrap]}><Text color="#fff">확인</Text></View>
        </TouchableOpacity>
      </View>
      {surveyIdx !== null && <SafeAreaView />}
    </View>
  );
}

Survey.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
