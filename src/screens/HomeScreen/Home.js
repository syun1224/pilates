import React, { useEffect, useState, useCallback } from 'react';
import {
  View, BackHandler, ScrollView, Picker, Image, SafeAreaView,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import LinearGradient from 'react-native-linear-gradient';

import { useDispatch, useSelector } from 'react-redux';

import { reservationDateRequest, filterRequest } from '../../store/actions';

import Header from '../../components/Header';
import SideMenu from './components/SideMenu';
import Calendar from './components/Calendar';
import CalendarModal from './components/CalendarModal';
import { REM } from '../../../constants/Config';
import { Text } from '../../components/material';


export default function HomeScreen({ navigation }) {
  const [focus, setFocus] = useState(0);
  const [date, setDate] = useState(null);
  const [slideIn, setSlideIn] = useState(false);
  const [hideHeaderLine, setHideHeaderLine] = useState(false);
  const dispatch = useDispatch();
  const { dateList = [], isFetching, error } = useSelector((state) => state.reservationDate, []);

  const styles = EStyleSheet.create({
    tabContainer: {
      flex: 1, backgroundColor: '#aaa', flexDirection: 'column',
    },
    tab: {
      backgroundColor: '#fff',
      width: '100%',
      height: '55rem',
      flexDirection: 'row',
    },
  });

  useEffect(() => {
    setHideHeaderLine(true);
    dispatch(reservationDateRequest());
    dispatch(filterRequest());
  }, []);

  useFocusEffect(
    useCallback(() => {
      const onBackPress = () => {
        if (slideIn) {
          setSlideIn(false);
          return true;
        }
        BackHandler.exitApp();
        return true;
      };

      BackHandler.addEventListener('hardwareBackPress', onBackPress);

      return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
    }, [slideIn]),
  );

  return (
    <>
      <View style={{ flex: 1 }}>
        <Header hideLine={hideHeaderLine} openMenu={() => setSlideIn(true)}>강남점</Header>
        <ScrollView
          style={{ flex: 1, zIndex: -1 }}
          onScroll={({ nativeEvent }) => {
            if (nativeEvent.contentOffset.y > 0) setHideHeaderLine(false);
            else setHideHeaderLine(true);
          }}
        >
          <View>
            <Image style={{ width: '100%', height: 150 * REM }} source={{ uri: 'http://thetravelnews.co.kr/wp-content/uploads/2019/07/%ED%81%AC%EA%B8%B0%EB%B3%80%ED%99%98%EB%B9%84%EC%9C%A8%ED%95%84%EB%9D%BC%ED%85%8C%EC%8A%A4-1.jpg' }} />
            <Text size={20} style={{ position: 'absolute', bottom: 10 * REM, left: 10 * REM }} weight="bold">{`${new Date().getMonth() + 1}월`}</Text>
          </View>
          <Calendar onDayPress={(dateObj) => { if (dateObj) setDate(dateObj.date); }} dateList={dateList} />
          <SafeAreaView />
        </ScrollView>
        <CalendarModal date={date} close={() => setDate(null)} />
        <SideMenu slideIn={slideIn} close={() => setSlideIn(false)} navigation={navigation} />
      </View>
    </>
  );
}

HomeScreen.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};
