import React, {
  useEffect, useState, useRef,
} from 'react';
import {
  View, Animated, TouchableWithoutFeedback, TouchableOpacity, SafeAreaView,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import { CommonActions } from '@react-navigation/native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Switch } from 'react-native-switch';

import { Text, Button } from '../../../components/material';
import { REM } from '../../../../constants/Config';

const defaultColor = '#faa';
const textColor = '#fff';

const styles = EStyleSheet.create({
  sideContainer: {
    height: '100%',
    width: '100%',
    position: 'absolute',
  },
  sideWrap: {
    width: '230rem',
    height: '100%',
    position: 'absolute',
    backgroundColor: '#fff',
  },
  tab: {
    backgroundColor: '$defaultColor',
    width: '100%',
    height: '55rem',
    flexDirection: 'row',
  },
  listWrap: {
    width: '100%',
    height: '40rem',
    paddingLeft: '20rem',
    justifyContent: 'center',
    borderBottomWidth: '1rem',
    borderBottomColor: 'rgb(190,190,190)',
  },
  headerWrap: {
    width: '100%',
    height: '100rem',
    paddingHorizontal: '10rem',
    backgroundColor: defaultColor,
    flexDirection: 'row',
  },
  tabHeaderWrap: {
    width: '100%',
    height: '40rem',
    paddingLeft: '15rem',
    justifyContent: 'center',
    backgroundColor: defaultColor,
  },
  switchWrap: {
    width: '100%',
    height: '40rem',
    flexDirection: 'row',
    alignItems: 'center',
  },
  switchTextWrap: {
    width: '180rem',
    height: '30rem',
    justifyContent: 'center',
    paddingLeft: '20rem',
  },
  infoWrap: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingBottom: '30rem',
    paddingLeft: '20rem',
  },
});

export default function SideMenu({ navigation, slideIn, close }) {
  const [visible, setVisible] = useState(false);
  const [classNotice, setClassNotice] = useState(false);
  const [emptyNotice, setEmptyNotice] = useState(false);
  const opacity = useRef(new Animated.Value(0)).current;
  const slide = useRef(new Animated.Value(-220 * REM)).current;

  useEffect(() => {
    if (slideIn) setVisible(true);
    setTimeout(() => Animated.parallel([
      Animated.timing(opacity, {
        toValue: slideIn ? 1 : 0,
        duration: 200,
        useNativeDriver: true,
      }),
      Animated.timing(slide, {
        toValue: slideIn ? 0 : -220 * REM,
        duration: 200,
        useNativeDriver: true,
      }),
    ]).start(() => {
      if (!slideIn) setVisible(false);
    }), 0);
  }, [slideIn]);

  const toggleClassNoticeSwitch = (e) => setClassNotice(e);
  const toggleEmptyNoticeSwitch = (e) => setEmptyNotice(e);

  if (visible) {
    return (
      <Animated.View style={styles.sideContainer}>
        <TouchableWithoutFeedback onPress={close}>
          <Animated.View style={[styles.sideContainer, { backgroundColor: '#0005', opacity }]} />
        </TouchableWithoutFeedback>
        <Animated.View style={[styles.sideWrap, {
          transform: [
            { translateX: slide },
          ],
        }]}
        >
          <View style={styles.headerWrap}>
            <View style={{ flex: 1, paddingTop: 20 * REM }}>
              <Text color={textColor}>OOO님</Text>
            </View>
            <View style={{ justifyContent: 'center' }}>
              <Button
                backgroundColor="#fff"
                height={30}
                width={60}
                border="#fff5 1 5"
                fontSize={13}
                color="#555"
                onPress={() => navigation.dispatch(
                  CommonActions.reset({
                    index: 0,
                    routes: [
                      { name: 'SignIn' },
                    ],
                  }),
                )}
              >Logout
              </Button>
            </View>
          </View>
          <List onPress={() => { navigation.navigate('MyPage'); close(); }}>마이페이지</List>
          <List onPress={() => { navigation.navigate('TimeTable'); close(); }}>시간표</List>
          <List onPress={() => { navigation.navigate('Notice'); close(); }}>공지사항</List>
          <List onPress={() => { navigation.navigate('Question'); close(); }}>문의사항</List>
          <List hideBottomLine onPress={() => { navigation.navigate('Survey'); close(); }}>만족도 조사</List>
          <View>
            <View style={styles.tabHeaderWrap}><Text color={textColor}>알람</Text></View>
            <View>
              <View style={[styles.switchWrap, { borderBottomColor: 'rgb(190,190,190)', borderBottomWidth: 1 * REM }]}>
                <View style={styles.switchTextWrap}><Text>수업시작 전 알람</Text></View>
                <Switch
                  circleSize={20 * REM}
                  barHeight={22 * REM}
                  circleBorderWidth={0}
                  switchWidthMultiplier={2 * REM}
                  backgroundInactive="#0005"
                  backgroundActive={defaultColor}
                  onValueChange={toggleClassNoticeSwitch}
                  value={classNotice}
                />
              </View>
              <View style={styles.switchWrap}>
                <View style={styles.switchTextWrap}><Text>공석알림</Text></View>
                <Switch
                  circleSize={20 * REM}
                  barHeight={22 * REM}
                  circleBorderWidth={0}
                  switchWidthMultiplier={2 * REM}
                  backgroundInactive="#0005"
                  backgroundActive={defaultColor}
                  onValueChange={toggleEmptyNoticeSwitch}
                  value={emptyNotice}
                />
              </View>
            </View>
          </View>
          <View>
            <View style={styles.tabHeaderWrap}><Text color={textColor}>약관 및 정책</Text></View>
            <List onPress={() => { navigation.navigate('Policy'); close(); }}>이용약관 / 정책</List>
          </View>
          <List onPress={() => { navigation.navigate('BranchInfo'); close(); }}>지점 안내</List>
          <View style={styles.infoWrap}>
            <Text color="#7779">{`버전 정보: ${'0.0.1'}`}</Text>
          </View>
        </Animated.View>
      </Animated.View>
    );
  }
  return null;
}

SideMenu.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
  }).isRequired,
  close: PropTypes.func,
  slideIn: PropTypes.bool,
};

SideMenu.defaultProps = {
  slideIn: false,
  close: () => {},
};


function List({ children, onPress, hideBottomLine }) {
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.listWrap, hideBottomLine && { borderBottomWidth: 0 }]}>
        <Text>{children}</Text>
      </View>
    </TouchableOpacity>
  );
}
List.propTypes = {
  children: PropTypes.string,
  onPress: PropTypes.func,
  hideBottomLine: PropTypes.bool,
};

List.defaultProps = {
  children: <Text>No Children</Text>,
  onPress: () => {},
  hideBottomLine: false,
};
