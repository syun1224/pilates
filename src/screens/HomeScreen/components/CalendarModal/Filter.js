import React, { useState, useRef, useEffect } from 'react';
import {
  View, TouchableWithoutFeedback, ScrollView, Dimensions, Image, TouchableOpacity,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';

// import { filterRequest } from '../../../../store/actions';
import { Button, Text } from '../../../../components/material';
import { REM } from '../../../../../constants/Config';

export default function Filter({ isOpen, close, setFilter }) {
  const { height, width } = Dimensions.get('window');
  const styles = EStyleSheet.create({
    pageContainer: {
      width,
      height,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.5)',
    },
    cardWrap: {
      paddingVertical: '25rem',
      justifyContent: 'center',
      alignItems: 'center',
    },
    card: {
      width: '320rem',
      height: '95%',
      borderRadius: '10rem',
      paddingBottom: '10rem',
      backgroundColor: '#fff',
    },
    filterContainer: {
      flex: 1,
      padding: '20rem',
    },
    filterTopWrap: {
      marginBottom: '20rem',
    },
    filterWrap: {
      width: '100%',
      // paddingHorizontal:'',
      paddingLeft: '30rem',
      paddingVertical: '10rem',
      // backgroundColor: '#0f0',
    },
    instructorWrap: {
      flexDirection: 'row',
      paddingBottom: '5rem',
    },
    instructor: {
      marginHorizontal: '7rem',
      justifyContent: 'center',
      alignItems: 'center',
    },
    instructorImage: {
      width: '50rem',
      height: '50rem',
      borderRadius: '25rem',
      marginBottom: '5rem',
    },
    typeWrap: {
      flexDirection: 'row',
      paddingBottom: '5rem',
    },
    type: {
      marginHorizontal: '7rem',
      width: '50rem',
      height: '30rem',
      borderRadius: '12rem',
      backgroundColor: '#fff',
      justifyContent: 'center',
      alignItems: 'center',
    },
    buttonWrap: {
      width: '100%',
      height: '70rem',
      justifyContent: 'center',
      alignItems: 'center',
    },
  });

  // const dispatch = useDispatch();
  const { data = {}, isFetching, error } = useSelector((state) => state.filter, null);
  const [instructorList, setInstructorList] = useState([]);
  const [typeList, setTypeList] = useState([]);
  const [instructorFilterList, setInstructorFilterList] = useState([]);
  const [typeFilterList, setTypeFilterList] = useState([]);

  // useEffect(() => {
  //   if (isOpen) {
  //     dispatch(filterRequest());
  //   }
  // }, [isOpen]);

  useEffect(() => {
    const newInstructorList = [];
    const newTypeList = [];
    if (data.instructorList) {
      data.instructorList.forEach((instructor, i) => {
        if (Number.isInteger(i / 4)) newInstructorList[Math.floor(i / 4)] = [];
        newInstructorList[Math.floor(i / 4)].push(instructor);
      });
    }
    if (data.typeList) {
      data.typeList.forEach((type, i) => {
        if (Number.isInteger(i / 4)) newTypeList[Math.floor(i / 4)] = [];
        newTypeList[Math.floor(i / 4)].push(type);
      });
    }
    setInstructorList([...newInstructorList]);
    setTypeList([...newTypeList]);
  }, [data]);

  if (isOpen) {
    return (
      <TouchableWithoutFeedback onPress={() => {
        close();
      }}
      >
        <View style={[styles.pageContainer, { position: 'absolute', backgroundColor: 'rgba(0,0,0,0)' }]}>
          <View style={[styles.cardWrap, { paddingVertical: 10 * REM }]}>
            <TouchableWithoutFeedback onPress={() => {}}>
              <View style={[styles.card, { backgroundColor: 'rgba(0,0,0,0.8)' }]}>
                <ScrollView style={styles.filterContainer}>
                  <View style={styles.filterTopWrap}>
                    <Text size={18} color="#fff" weight="bold">강사님</Text>
                    <View style={styles.filterWrap}>
                      {instructorList && instructorList.length !== 0 && instructorList.map((instructorWrap) => (
                        <View style={styles.instructorWrap}>
                          {instructorWrap.map((instructor) => (
                            <TouchableOpacity onPress={() => {
                              if (instructorFilterList.includes(instructor.id)) {
                                const index = instructorFilterList.indexOf(instructor.id);
                                instructorFilterList.splice(index, 1);
                                setInstructorFilterList([...instructorFilterList]);
                              } else setInstructorFilterList([...instructorFilterList, instructor.id]);
                            }}
                            >
                              <View key={instructor.id} style={styles.instructor}>
                                <Image style={[styles.instructorImage, instructorFilterList.includes(instructor.id) && { borderColor: '#090', borderWidth: 2 }]} source={{ uri: instructor.image }} />
                                <Text size={13} color="#fff" weight="bold">{instructor.name}</Text>
                                <Text size={12} color="#fff">강사님</Text>
                              </View>
                            </TouchableOpacity>
                          ))}
                        </View>
                      ))}
                    </View>
                  </View>
                  <View>
                    <Text size={18} color="#fff" weight="bold">수업</Text>
                    <View style={styles.filterWrap}>
                      {typeList && typeList.length !== 0 && typeList.map((typeWrap) => (
                        <View style={styles.typeWrap}>
                          {typeWrap.map((type) => (
                            <TouchableOpacity onPress={() => {
                              if (typeFilterList.includes(type.id)) {
                                const index = typeFilterList.indexOf(type.id);
                                typeFilterList.splice(index, 1);
                                setTypeFilterList([...typeFilterList]);
                              } else setTypeFilterList([...typeFilterList, type.id]);
                            }}
                            >
                              <View style={[styles.type, typeFilterList.includes(type.id) && { backgroundColor: '#090' }]}>
                                <Text size={13} color={typeFilterList.includes(type.id) ? '#fff' : '#000'}>{type.name}</Text>
                              </View>
                            </TouchableOpacity>
                          ))}
                        </View>
                      ))}
                    </View>
                  </View>
                </ScrollView>
                <View style={styles.buttonWrap}>
                  {/* {(instructorFilterList.length !== 0 && typeFilterList.length !== 0)
                    ? ( */}
                  <Button
                    width={120}
                    height={40}
                    backgroundColor="#090"
                    color="#fff"
                    border="1 15"
                    onPress={() => {
                      setFilter({ instructor: [...instructorFilterList], type: [...typeFilterList] });
                      close();
                    }}
                  >선택 완료
                  </Button>
                  {/* )
                    : <Button width={120} height={40} backgroundColor="rgba(190,190,190,5)" color="#fff" border="1 15">선택 완료</Button>} */}
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
  return <></>;
}

Filter.defaultProps = {
  isOpen: false,
  close: () => {},
  setFilter: () => {},
};

Filter.propTypes = {
  isOpen: PropTypes.bool,
  close: PropTypes.func,
  setFilter: PropTypes.func,
};
