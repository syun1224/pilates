import React, { useState, useRef, useEffect } from 'react';
import {
  View, TouchableWithoutFeedback, Dimensions, ScrollView, Modal, TouchableOpacity, Alert, Image, Animated,
} from 'react-native';
import Svg, { Line } from 'react-native-svg';
import { useDispatch, useSelector } from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign';
import PropTypes from 'prop-types';
import moment from 'moment';
import ViewPager from '@react-native-community/viewpager';
import EStyleSheet from 'react-native-extended-stylesheet';
import LinearGradient from 'react-native-linear-gradient';

import { groupClassRequest } from '../../../../store/actions';
import { Button, Text } from '../../../../components/material';
import { REM } from '../../../../../constants/Config';
import List from './GroupList';
import Filter from './Filter';
import TimeModal from './TimeModal';


export default function CalendarModal({ date, close }) {
  const dayOfWeek = ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'];
  const { height, width } = Dimensions.get('window');
  const styles = EStyleSheet.create({
    pageContainer: {
      width,
      height,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.5)',
    },
    cardWrap: {
      paddingVertical: '25rem',
      justifyContent: 'center',
      alignItems: 'center',
    },
    card: {
      width: '320rem',
      height: '95%',
      borderRadius: '10rem',
      // paddingBottom: '10rem',
      backgroundColor: '#fff',
    },
    cardBackground: {
      position: 'absolute', height, width,
    },
    cardHeader: {
      width: '100%',
      height: '70rem',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: '20rem',
      flexDirection: 'row',
      borderBottomWidth: '0.2rem',
      borderBottomColor: '#000',
    },
    cardHeaderTextWrap: {
      height: '70rem',
      alignItems: 'center',
      flexDirection: 'row',
    },
    listWrap: {
      flex: 1,
      borderBottomStartRadius: '10rem',
      // justifyContent: 'center',
      // backgroundColor: '#000',
    },
    instructorListWrap: {
      borderTopColor: '#0005',
      borderTopWidth: '0.5rem',
      width: '100%',
      height: '100rem',
      // justifyContent: 'center',
    },
    instructorList: {
      paddingTop: '10rem',
    },
  });

  const viewPagerRef = useRef();
  const dispatch = useDispatch();
  const { groupClasses, isFetching, error } = useSelector((state) => state.groupClass, null);
  const { dateList = [] } = useSelector((state) => state.reservationDate, []);

  const [classList, setClassList] = useState([]);
  const [openFilter, setOpenFilter] = useState(false);
  const [filter, setFilter] = useState({});
  const [isPersonal, setIsPersonal] = useState(false);
  const [startTime, setStartTime] = useState(null);
  const [endTime, setEndTime] = useState(null);
  const [time, setTime] = useState(null);

  // 초기 데이터 렌더링
  useEffect(() => {
    if (date && dateList.length !== 0 && classList.length === 0) {
      setClassList([...dateList.map((dateObj) => ({ id: 0, date: dateObj.date, list: [] }))]);
      dispatch(groupClassRequest(date));
      setTimeout(() => viewPagerRef.current.setPage(
        dateList.findIndex((dateObj) => date === dateObj.date),
      ), 0);
    } else setFilter({});
  }, [date]);

  // 필터
  useEffect(() => {
    if (classList.length !== 0 && groupClasses) {
      const classIndex = classList.findIndex((obj) => obj.date === groupClasses.date);
      if ((!filter.instructor && !filter.type) || (filter.instructor.length === 0 && filter.type === 0)) {
        setClassList([...classList.slice(0, classIndex), groupClasses, ...classList.slice(classIndex + 1)]);
      } else {
        const newGroupClasses = JSON.parse(JSON.stringify(groupClasses));
        let firstFilterList = [];
        let lastFilterList = [];
        newGroupClasses.list.forEach((obj, i) => {
          if (filter.instructor.length === 0) {
            firstFilterList = newGroupClasses.list;
            return;
          }
          if (filter.instructor.includes(obj.instructor.id)) firstFilterList.push(obj);
        });
        firstFilterList.forEach((obj, i) => {
          if (filter.type.length === 0) {
            lastFilterList = firstFilterList;
            return;
          }
          if (filter.type.includes(obj.type.id)) lastFilterList.push(obj);
        });
        setClassList([...classList.slice(0, classIndex), { ...newGroupClasses, list: lastFilterList }, ...classList.slice(classIndex + 1)]);
      }
    }
  }, [groupClasses, filter]);

  useEffect(() => {
  }, [isFetching]);

  return (
    <Modal
      visible={!!date}
      transparent
      onRequestClose={() => {
        close();
        setClassList([]);
      }}
    >
      <ViewPager
        ref={viewPagerRef}
        style={styles.pageContainer}
        pageMargin={-37 * REM}
        onPageSelected={({ nativeEvent }) => {
          if (classList[nativeEvent.position]) {
            dispatch(groupClassRequest(classList[nativeEvent.position].date));
          }
        }}
      >
        {
          isPersonal
            ? ( // 개인
              <View style={styles.cardWrap}>
                <TouchableWithoutFeedback onPress={() => {
                  close();
                  setClassList([]);
                }}
                >
                  <View style={styles.cardBackground} />
                </TouchableWithoutFeedback>
                <View style={styles.card}>
                  <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderTextWrap}>
                      <Text size={20}>{`${1}월 ${1}일 `}</Text>
                      <Text size={16}>{`${'수요일'}`}</Text>
                    </View>
                    <View style={styles.cardHeaderTextWrap}>
                      <TouchableOpacity style={{ marginRight: 5 }} onPress={() => setOpenFilter(true)}>
                        <AntDesign name="filter" size={30 * REM} />
                      </TouchableOpacity>
                      <Button
                        height={20}
                        width={35}
                        backgroundColor="#099"
                        fontSize={10}
                        border="0 15"
                        color="#fff"
                        fontWeight="200"
                        onPress={() => {
                          setIsPersonal(!isPersonal);
                        }}
                      >개인
                      </Button>
                    </View>
                  </View>
                  <ScrollView style={styles.listWrap}>
                    <View style={{ paddingVertical: 30 * REM }}>
                      {[9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23].map((hour) => (
                        <View style={{ flexDirection: 'row' }}>
                          <View style={{
                            paddingLeft: 10 * REM, height: 40 * REM, width: 50 * REM, top: -10 * REM,
                          }}
                          ><Text>{hour}:00</Text>
                          </View>

                          <View style={{
                            height: 40 * REM, width: '82%', justifyContent: 'center',
                          }}
                          >
                            <TouchableOpacity onPress={() => {
                              setStartTime(hour < 10 ? `0${hour}:00` : `${hour}:00`);
                              setEndTime(hour + 1 < 10 ? `0${hour + 1}:00` : `${hour + 1}:00`);
                            }}
                            >
                              <View style={{ height: 40 * REM, width: '100%' }}>
                                <Svg height="1" width="100%">
                                  <Line
                                    stroke="black"
                                    strokeWidth="0.5"
                                    x1="0"
                                    y1="0"
                                    x2="100%"
                                    y2="0"
                                  />
                                </Svg>
                              </View>
                            </TouchableOpacity>
                          </View>
                        </View>
                      ))}
                      {
                        [
                          {
                            id: 0, startAt: '10:00', endAt: '11:00', isMine: false,
                          },
                          {
                            id: 0, startAt: '12:00', endAt: '13:00', isMine: true,
                          },
                          {
                            id: 0, startAt: '13:00', endAt: '14:50', isMine: false,
                          },
                          {
                            id: 0, startAt: '17:00', endAt: '18:30', isMine: false,
                          },
                          {
                            id: 0, startAt: '20:00', endAt: '22:00', isMine: false,
                          },
                        ].map((data) => {
                          const { startAt, endAt, isMine } = data;
                          const defaultBarTime = moment('2020-01-01 09:00');
                          const startBarTime = moment(`2020-01-01 ${startAt}`);
                          const endBerTime = moment(`2020-01-01 ${endAt}`);
                          const diffPosition = moment.duration(startBarTime.diff(defaultBarTime)).asMinutes();
                          const diffHeight = moment.duration(endBerTime.diff(startBarTime)).asMinutes();
                          return (
                            <View style={{ position: 'absolute', top: 30 * REM + (((40 * REM) / 60) * diffPosition) }}>
                              <View style={{
                                marginLeft: 50 * REM, width: 263 * REM, height: (((40 * REM) / 60) * diffHeight), backgroundColor: isMine ? '#9f9' : '#f9f',
                              }}
                              />
                            </View>
                          );
                        })
                      }
                    </View>
                  </ScrollView>
                  <View style={styles.instructorListWrap}>
                    <ScrollView style={styles.instructorList} horizontal>
                      {[{}, {}, {}, {}, {}].map((obj) => (
                        <View style={{ height: '100%', alignItems: 'center', marginHorizontal: 10 * REM }}>
                          <Image style={{
                            height: 50 * REM, width: 50 * REM, borderRadius: 25 * REM, backgroundColor: '#ff04', marginBottom: 5 * REM,
                          }}
                          />
                          <Text size={13}>김언리</Text>
                          <Text size={10}>선생님</Text>
                        </View>
                      ))}
                    </ScrollView>
                  </View>
                </View>
              </View>
            )
            : classList.length !== 0 && classList.map((obj) => { // 그룹
              if (!obj && !obj.id) {
                return (
                  <View style={styles.cardWrap}>
                    <View style={styles.card} />
                  </View>
                );
              }
              const { list = [] } = obj;
              const month = obj.date.split('-')[1];
              const day = obj.date.split('-')[2];
              return (
                <View style={styles.cardWrap}>
                  <TouchableWithoutFeedback onPress={() => {
                    close();
                    setClassList([]);
                  }}
                  >
                    <View style={styles.cardBackground} />
                  </TouchableWithoutFeedback>
                  <View style={styles.card}>
                    <View style={styles.cardHeader}>
                      <View style={styles.cardHeaderTextWrap}>
                        <Text size={20}>{`${month * 1}월 ${day * 1}일 `}</Text>
                        <Text size={16}>{`${dayOfWeek[moment(obj.date).weekday()]}`}</Text>
                      </View>
                      <View style={styles.cardHeaderTextWrap}>
                        <TouchableOpacity style={{ marginRight: 5 }} onPress={() => setOpenFilter(true)}>
                          <AntDesign name="filter" size={30 * REM} />
                        </TouchableOpacity>
                        <Button
                          height={20}
                          width={35}
                          backgroundColor="#090"
                          fontSize={10}
                          border="0 15"
                          color="#fff"
                          fontWeight="200"
                          onPress={() => {
                            setIsPersonal(!isPersonal);
                          }}
                        >그룹
                        </Button>
                      </View>
                    </View>
                    <View style={styles.listWrap}>
                      <ScrollView style={{ flex: 1 }}>
                        {list.map((data) => {
                          const {
                            startAt, endAt, instructor, type, capacity, headcount,
                          } = data;
                          return (
                            <List
                              id={data.id}
                              startAt={startAt}
                              endAt={endAt}
                              instructor={instructor}
                              type={type.name}
                              capacity={capacity}
                              headcount={headcount}
                            />
                          );
                        })}
                      </ScrollView>
                    </View>
                  </View>
                </View>
              );
            })
        }
      </ViewPager>
      <Filter isOpen={openFilter} close={() => setOpenFilter(false)} setFilter={setFilter} />
      {isFetching && <View style={[styles.pageContainer, { position: 'absolute', backgroundColor: 'rgba(0,0,0,0.5)' }]} />}
      <TimeModal startTime={startTime} endTime={endTime} setEndTime={setEndTime} setStartTime={setStartTime} />
    </Modal>
  );
}

CalendarModal.defaultProps = {
  date: false,
  close: () => {},
};

CalendarModal.propTypes = {
  date: PropTypes.string,
  close: PropTypes.func,
};
