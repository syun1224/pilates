import React, { useState, useRef, useEffect } from 'react';
import {
  View, TouchableWithoutFeedback, Dimensions, ScrollView, Modal, TouchableOpacity, Alert, Image, Animated,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import PropTypes from 'prop-types';
import moment from 'moment';
import EStyleSheet from 'react-native-extended-stylesheet';
import { WheelPicker, TimePicker, DatePicker } from 'react-native-wheel-picker-android';

import { Button, Text } from '../../../../components/material';
import { REM } from '../../../../../constants/Config';

export default function TimeModal({
  startTime, endTime, setStartTime, setEndTime,
}) {
  const styles = EStyleSheet.create({});

  const [start, setStart] = useState(null);
  const [end, setEnd] = useState(null);
  const slide = useRef(new Animated.Value(-250)).current;

  useEffect(() => {
    if (!!startTime && !!endTime) {
      setStart(startTime);
      setEnd(endTime);
      Animated.timing(slide, {
        toValue: 0,
        duration: 300,
        useNativeDriver: false,
      }).start();
    } else {
      Animated.timing(slide, {
        toValue: -250,
        duration: 300,
        useNativeDriver: false,
      }).start(() => {
        setStart(null);
        setEnd(null);
      });
    }
  }, [startTime, endTime]);

  return (
    <Animated.View style={{
      width: '100%', height: 150, position: 'absolute', bottom: slide, backgroundColor: '#fff', borderTopEndRadius: 20, borderTopStartRadius: 20,
    }}
    >
      <View style={{ flex: 1, paddingTop: 20 }}>
        <View style={{ marginLeft: 20 }}>
          <Text size={20}>예약 시간</Text>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 10 }}>
          <View style={{
            height: 32, width: 200, overflow: 'hidden',
          }}
          >
            <View style={{ position: 'absolute', left: 100 }}>
              <Text size={20}>:</Text>
            </View>
            {start && (
            <TimePicker
              style={{
                width: 100, height: 120, top: -63,
              }}
              selectedItemTextSize={22}
              initDate={new Date(`2020-01-01T${startTime}:00`)}
              hideIndicator
              format24
              onTimeSelected={(time) => {
                setStart(`${time.getHours() < 10 ? `0${time.getHours()}` : time.getHours()}:${time.getMinutes() < 10 ? `0${time.getMinutes()}` : time.getMinutes()}`);
              }}
            />
            )}
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center' }}>
            <Text size={20}>~</Text>
          </View>
          <View style={{
            height: 32, width: 200, overflow: 'hidden',
          }}
          >
            <View style={{ position: 'absolute', left: 100 }}>
              <Text size={20}>:</Text>
            </View>
            {end && (
            <TimePicker
              style={{
                width: 100, height: 120, top: -63,
              }}
              selectedItemTextSize={22}
            // indicatorWidth={30}
            // itemTextSize={20}
              initDate={new Date(`2020-01-01T${endTime}:00`)}
              hideIndicator
              format24
              onTimeSelected={(time) => {
                setEnd(`${time.getHours() < 10 ? `0${time.getHours()}` : time.getHours()}:${time.getMinutes() < 10 ? `0${time.getMinutes()}` : time.getMinutes()}`);
              }}
            />
            )}
          </View>
        </View>
      </View>
      <View style={{ flexDirection: 'row', justifyContent: 'flex-end', paddingRight: 20 }}>
        <Button onPress={() => {
          setStartTime(null);
          setEndTime(null);
        }}
        >예약
        </Button>
        <Button onPress={() => {
          setStartTime(null);
          setEndTime(null);
        }}
        >취소
        </Button>

      </View>
    </Animated.View>
  );
}


TimeModal.defaultProps = {
  startTime: null,
  endTime: null,
  setStartTime: () => {},
  setEndTime: () => {},
};

TimeModal.propTypes = {
  startTime: PropTypes.string,
  endTime: PropTypes.string,
  setStartTime: PropTypes.func,
  setEndTime: PropTypes.func,
};
