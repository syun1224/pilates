import React, { useState, useRef, useEffect } from 'react';
import {
  View, TouchableWithoutFeedback, Dimensions, ScrollView, Modal, TouchableOpacity, Alert, Image, Animated,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import AntDesign from 'react-native-vector-icons/AntDesign';
import PropTypes from 'prop-types';
import moment from 'moment';
import ViewPager from '@react-native-community/viewpager';
import EStyleSheet from 'react-native-extended-stylesheet';
import LinearGradient from 'react-native-linear-gradient';

import { groupClassRequest, personalClassRequest } from '../../../../store/actions';
import { Button, Text } from '../../../../components/material';
import { REM } from '../../../../../constants/Config';
import List from './List';
import Filter from './Filter';


export default function CalendarModal({ date, close }) {
  const dayOfWeek = ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'];
  const { height, width } = Dimensions.get('window');
  const styles = EStyleSheet.create({
    pageContainer: {
      width,
      height,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0,0,0,0.5)',
    },
    cardWrap: {
      paddingVertical: '25rem',
      justifyContent: 'center',
      alignItems: 'center',
    },
    card: {
      width: '320rem',
      height: '95%',
      borderRadius: '10rem',
      backgroundColor: '#fff',
    },
    cardBackground: {
      position: 'absolute', height, width,
    },
    cardHeader: {
      width: '100%',
      height: '70rem',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingHorizontal: '20rem',
      flexDirection: 'row',
      borderBottomWidth: '0.2rem',
      borderBottomColor: '#000',
    },
    cardHeaderTextWrap: {
      height: '70rem',
      alignItems: 'center',
      flexDirection: 'row',
    },
    listWrap: {
      flex: 1,
      borderBottomStartRadius: '10rem',
      // justifyContent: 'center',
      // backgroundColor: '#000',
    },
    instructorListWrap: {
      borderTopColor: '#0005',
      borderTopWidth: '0.5rem',
      width: '100%',
      height: '100rem',
      // justifyContent: 'center',
    },
    instructorList: {
      paddingTop: '10rem',
    },
  });

  const viewPagerRef = useRef();
  const dispatch = useDispatch();
  const { groupClasses, isFetching: isGroupFetching, error: groupError } = useSelector((state) => state.groupClass, null);
  const { personalClasses, isFetching: isPersonalFetching, error: personalError } = useSelector((state) => state.personalClass, null);
  const { dateList = [] } = useSelector((state) => state.reservationDate, []);

  const [classList, setClassList] = useState([]);
  const [selectDate, setSelectDate] = useState(null);
  const [openFilter, setOpenFilter] = useState(false);
  const [filter, setFilter] = useState({});
  const [isPersonal, setIsPersonal] = useState(false);
  const [isInit, setIsInit] = useState(false);

  // 초기 데이터 렌더링
  useEffect(() => {
    if (date && dateList.length !== 0 && classList.length === 0) {
      setIsInit(true);
      setClassList([...dateList.map((dateObj) => ({ id: 0, date: dateObj.date, list: [] }))]);
      if (isPersonal) dispatch(personalClassRequest(date));
      else dispatch(groupClassRequest(date));
      setTimeout(() => {
        viewPagerRef.current.setPage(
          dateList.findIndex((dateObj) => date === dateObj.date),
        );
        setTimeout(() => setIsInit(false), 600);
      }, 0);
      setSelectDate(date);
    } else {
      setFilter({});
      setSelectDate(null);
    }
  }, [date]);

  useEffect(() => {
    if (!isInit) {
      if (isPersonal) dispatch(personalClassRequest(selectDate));
      else dispatch(groupClassRequest(selectDate));
    }
  }, [selectDate]);

  useEffect(() => {
    if (classList.length !== 0) {
      setTimeout(() => {
        if (isPersonal) dispatch(personalClassRequest(selectDate));
        else dispatch(groupClassRequest(selectDate));
      }, 0);
    }
  }, [isPersonal]);

  useEffect(() => {
    if (classList.length !== 0 && personalClasses) {
      const classIndex = classList.findIndex((obj) => obj.date === selectDate);
      setClassList([...classList.slice(0, classIndex), personalClasses, ...classList.slice(classIndex + 1)]);
    }
  }, [personalClasses]);

  // 필터
  useEffect(() => {
    console.log('groupClasses', groupClasses);
    if (classList.length !== 0 && groupClasses) {
      const classIndex = classList.findIndex((obj) => obj.date === selectDate);
      if ((!filter.instructor && !filter.type) || (filter.instructor.length === 0 && filter.type === 0)) {
        setClassList([...classList.slice(0, classIndex), groupClasses, ...classList.slice(classIndex + 1)]);
      } else {
        const newClasses = JSON.parse(JSON.stringify(groupClasses));
        let firstFilterList = [];
        let lastFilterList = [];

        console.log('newClasses', newClasses);
        newClasses.list.forEach((obj, i) => {
          if (filter.instructor.length === 0) {
            firstFilterList = newClasses.list;
            return;
          }
          if (filter.instructor.includes(obj.instructor.id)) firstFilterList.push(obj);
        });
        firstFilterList.forEach((obj, i) => {
          if (filter.type.length === 0) {
            lastFilterList = firstFilterList;
            return;
          }
          if (filter.type.includes(obj.type.id)) lastFilterList.push(obj);
        });
        setClassList([...classList.slice(0, classIndex), { ...newClasses, list: lastFilterList }, ...classList.slice(classIndex + 1)]);
      }
    }
  }, [groupClasses, filter]);

  useEffect(() => {
    console.log('...fetching', isGroupFetching, isPersonalFetching);
  }, [isGroupFetching, isPersonalFetching]);

  return (
    <Modal
      visible={!!date}
      transparent
      onRequestClose={() => {
        close();
        setClassList([]);
      }}
    >
      <ViewPager
        ref={viewPagerRef}
        style={styles.pageContainer}
        pageMargin={-37 * REM}
        onPageSelected={({ nativeEvent }) => {
          if (classList[nativeEvent.position]) {
            setSelectDate(classList[nativeEvent.position].date);
          }
        }}
      >
        {
          classList.length !== 0 && classList.map((obj) => { // 그룹
            if (!obj && !obj.id) {
              return (
                <View style={styles.cardWrap}>
                  <View style={[styles.card, { justifyContent: 'center', alignItems: 'center' }]}>
                    <Text>오류가 발생했습니다</Text>
                  </View>
                </View>
              );
            }
            const { list = [] } = obj;
            const month = obj.date.split('-')[1];
            const day = obj.date.split('-')[2];
            return (
              <View style={styles.cardWrap}>
                <TouchableWithoutFeedback onPress={() => {
                  close();
                  setClassList([]);
                }}
                >
                  <View style={styles.cardBackground} />
                </TouchableWithoutFeedback>
                <View style={styles.card}>
                  <View style={styles.cardHeader}>
                    <View style={styles.cardHeaderTextWrap}>
                      <Text size={20}>{`${month * 1}월 ${day * 1}일 `}</Text>
                      <Text size={16}>{`${dayOfWeek[moment(obj.date).weekday()]}`}</Text>
                    </View>
                    <View style={styles.cardHeaderTextWrap}>
                      {!isPersonal && (
                      <TouchableOpacity style={{ marginRight: 5 }} onPress={() => setOpenFilter(true)}>
                        <AntDesign name="filter" size={30 * REM} />
                      </TouchableOpacity>
                      )}
                      <Button
                        height={20}
                        width={35}
                        backgroundColor={isPersonal ? '#099' : '#090'}
                        fontSize={10}
                        border="0 15"
                        color="#fff"
                        fontWeight="200"
                        onPress={() => {
                          setClassList([...dateList.map((dateObj) => ({ id: 0, date: dateObj.date, list: [] }))]);
                          setIsPersonal(!isPersonal);
                        }}
                      >
                        {isPersonal ? '개인' : '그룹'}
                      </Button>
                    </View>
                  </View>
                  <View style={styles.listWrap}>
                    {
                        list.length === 0 ? (
                          <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                            <Text>해당하는 수업이 없습니다.</Text>
                          </View>
                        ) : (
                          <ScrollView style={{ flex: 1 }} contentContainerStyle={{ paddingTop: 10 * REM }}>
                            {list.map((data) => {
                              const {
                                startAt, endAt, instructor, type, capacity, headcount, isReservation, isNotification, isConfirmation,
                              } = data;
                              return (
                                <List
                                  id={data.id}
                                  startAt={startAt}
                                  endAt={endAt}
                                  instructor={instructor}
                                  type={type ? type.name : '개인'}
                                  capacity={capacity}
                                  headcount={headcount}
                                  isConfirmation={isConfirmation}
                                  isPersonal={isPersonal}
                                  isReservation={isReservation}
                                  isNotification={isNotification}
                                />
                              );
                            })}
                          </ScrollView>
                        )
                      }
                  </View>
                </View>
              </View>
            );
          })
        }
      </ViewPager>
      <Filter isOpen={openFilter} close={() => setOpenFilter(false)} setFilter={setFilter} />
      {(isGroupFetching || isPersonalFetching) && <View style={[styles.pageContainer, { position: 'absolute', backgroundColor: 'rgba(0,0,0,0.5)' }]} />}
    </Modal>
  );
}

CalendarModal.defaultProps = {
  date: false,
  close: () => {},
};

CalendarModal.propTypes = {
  date: PropTypes.string,
  close: PropTypes.func,
};
