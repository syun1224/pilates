import React, { useState, useEffect } from 'react';
import {
  View, Image, TouchableOpacity, Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';

import { Button, Text } from '../../../../components/material';
import { REM } from '../../../../../constants/Config';

const styles = EStyleSheet.create({
  listWrap: {
    width: '300rem',
    height: '90rem',
    alignSelf: 'center',
    marginBottom: '10rem',
    borderWidth: '1rem',
    borderColor: '#9996',
    borderRadius: '10rem',
    alignItems: 'center',
    flexDirection: 'row',
  },
  list: {
    flex: 1,
  },
  squareWrap: {
    width: '90rem',
    height: '90rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
  square: {
    width: '77rem',
    height: '77rem',
    borderRadius: '10rem',
  },
  infoWrap: {
    flex: 1,
    height: '100%',
    paddingVertical: '10rem',
    justifyContent: 'space-between',
  },
});


const List = React.memo((props) => {
  const {
    id, startAt, endAt, instructor, type, capacity, headcount, isReservation, isNotification, isPersonal, isConfirmation,
  } = props;
  const [button, setButton] = useState({ text: '', color: '#fff', onPress: () => {} });


  useEffect(() => {
    if (isPersonal) {
      if (isConfirmation) {
        setButton({ text: '예약취소', color: '#f69', onPress: () => {} });
      } else {
        setButton({ text: '예약확정', color: '#090', onPress: () => {} });
      }
    } else if (capacity > headcount) {
      if (isReservation) {
        setButton({ text: '예약취소', color: '#f69', onPress: () => {} });
      } else {
        setButton({ text: '예약하기', color: '#090', onPress: () => {} });
      }
    } else if (isNotification) {
      setButton({ text: '알람취소', color: '#555', onPress: () => {} });
    } else {
      setButton({ text: '공석알림', color: '#555', onPress: () => {} });
    }
  }, [props]);


  return (
    <View key={id} style={styles.listWrap}>
      <View style={styles.squareWrap}>
        <Image source={{ uri: instructor.image }} style={styles.square} />
      </View>
      <View style={styles.infoWrap}>
        <Text>{`${startAt} ~ ${endAt}`}</Text>
        <Text color="#090">{type}</Text>
        <Text>{instructor.name}</Text>
      </View>
      <View style={styles.squareWrap}>
        <Button style={[styles.square]} onPress={() => button.onPress} backgroundColor={button.color} color="#fff">{button.text}</Button>
      </View>
    </View>
  );
});

List.defaultProps = {
  id: 0,
  startAt: '',
  endAt: '',
  instructor: '',
  type: '',
  capacity: 0,
  headcount: 0,
  isReservation: false,
  isNotification: false,
  isPersonal: false,
  isConfirmation: false,
};

List.propTypes = {
  id: PropTypes.number,
  startAt: PropTypes.string,
  endAt: PropTypes.string,
  instructor: PropTypes.string,
  type: PropTypes.string,
  capacity: PropTypes.number,
  headcount: PropTypes.number,
  isReservation: PropTypes.bool,
  isNotification: PropTypes.bool,
  isPersonal: PropTypes.bool,
  isConfirmation: PropTypes.bool,
};


export default List;
