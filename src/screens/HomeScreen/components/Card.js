import React from 'react';
import {
  View, Image,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Button, Text } from '../../../components/material';

export default function Card(props) {
  const styles = EStyleSheet.create({
    cardWrap: {
      width: '345rem',
      height: '96rem',
      backgroundColor: '#fff',
      marginBottom: '4rem',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
    },
    profile: {
      width: '81rem',
      height: '83rem',
      backgroundColor: '#fcf',
      marginRight: '7rem',
    },
    infoWrap: {
      width: '243rem',
      height: '83rem',
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    buttonWrap: {
      height: '100%',
      justifyContent: 'space-between',
    },
  });
  return (
    <View style={styles.cardWrap}>
      <Image style={styles.profile} />
      <View style={styles.infoWrap}>
        <View>
          <View><Text>시간</Text></View>
          <Text>수업내용</Text>
          <Text>강사</Text>
          <Text>수업인원</Text>
        </View>
        <View style={styles.buttonWrap}>
          <Button width={60} backgroundColor="#777" border="0.5 #000">공석알림</Button>
          <Button width={60} backgroundColor="#777" border="0.5 #000">예약상세</Button>
        </View>
      </View>
    </View>
  );
}
