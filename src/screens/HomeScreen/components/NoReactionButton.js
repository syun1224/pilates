import React from 'react';
import {
  View, TouchableWithoutFeedback,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';
import PropTypes from 'prop-types';

import { Text } from '../../../components/material';

export default function NoReactionButton(props) {
  const {
    paddingHorizontal, height, children, onPress, color,
  } = props;
  const styles = EStyleSheet.create({
    button: {
      width: null,
      paddingHorizontal: `${paddingHorizontal}rem`,
      height: height ? `${height}rem` : '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={styles.button}>
        <Text color={color}>{children}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
}

NoReactionButton.defaultProps = {
  paddingHorizontal: 0,
  height: null,
  children: '',
  color: '#000',
  onPress: () => {},
};

NoReactionButton.propTypes = {
  paddingHorizontal: PropTypes.number,
  height: PropTypes.number,
  children: PropTypes.string,
  color: PropTypes.string,
  onPress: PropTypes.func,
};
