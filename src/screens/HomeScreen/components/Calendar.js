import React, { useState } from 'react';
import {
  View, TouchableOpacity,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import moment from 'moment';
import { Text } from '../../../components/material';
import { REM } from '../../../../constants/Config';

export default function Calendar({ onDayPress }) {
  const { dateList = [] } = useSelector((state) => state.reservationDate, []);
  // 달력 포멧 생성
  const [calendar, setCalendar] = useState([]);
  const dateArr = [];
  const dayOfWeekArr = ['일', '월', '화', '수', '목', '금', '토'];
  const current = moment();
  const prevDate = moment().startOf('months').startOf('week');
  const nextDate = moment().endOf('months').endOf('week').add(7, 'days');
  let number = 0;

  while (prevDate.isBefore(nextDate)) {
    if (Number.isInteger(number / 7)) dateArr[Math.floor(number / 7)] = [];
    dateArr[Math.floor(number / 7)].push({
      date: prevDate.format('YYYY-MM-DD'),
      day: prevDate.date(),
      month: prevDate.month() + 1,
      year: prevDate.year(),
      dayOfWeek: prevDate.days(),
    });
    number += 1;
    prevDate.add(1, 'days');
  }
  if (calendar.length === 0 && number >= 42) setCalendar([...dateArr]);
  // 정의
  // const onPress = useCallback(
  //   (e) => {
  //     const { name, value } = e.target;
  //     setInputs({
  //       ...inputs,
  //       [name]: value,
  //     });
  //   },
  //   [inputs],
  // );
  return (
    <View style={{ width: '100%', minHeight: 600, backgroundColor: '#fff' }}>
      <View style={{
        width: '100%', height: 30, flexDirection: 'row', backgroundColor: '#8882',
      }}
      >
        {dayOfWeekArr.map((str) => (
          <View style={{
            width: `${100 / 7}%`, flex: 1, justifyContent: 'center', alignItems: 'center',
          }}
          >
            <Text>{str}</Text>
          </View>
        ))}
      </View>
      <View>
        {calendar.map((weekArr = []) => (
          <View style={{ flexDirection: 'row' }}>
            {weekArr.map((dateObj) => {
              let textColor = '#000';
              if (dateObj.day === current.date()) textColor = '#09d';
              if (dateObj.month !== current.months() + 1) textColor = '#999';
              const index = dateList.findIndex((dateListObj) => dateListObj.date === dateObj.date);
              return (
                <View
                  style={{
                    width: `${100 / 7}%`, height: 90, borderWidth: 0.5, borderColor: '#9992',
                  }}
                >
                  <TouchableOpacity
                    style={{ width: '100%', height: '100%' }}
                    onPress={() => {
                      if (index !== -1) onDayPress(dateObj);
                    }}
                  >
                    <View style={{ flex: 1, alignItems: 'center' }}>
                      <Text color={textColor}>{dateObj.day}</Text>
                      {
                      (index !== -1 && dateList[index] && (dateList[index].reservationList.length !== 0))
                      && dateList[index].reservationList.map((obj) => (
                        <View style={{
                          width: '100%', height: 17 * REM, borderRadius: 5 * REM, backgroundColor: '#0095', justifyContent: 'center', alignItems: 'center', marginBottom: 2 * REM,
                        }}
                        ><Text limit={9} size={9}>{obj.time} {obj.type && obj.type.name}</Text>
                        </View>
                      ))
                    }


                    </View>
                  </TouchableOpacity>
                </View>
              );
            })}
          </View>
        ))}
      </View>
    </View>
  );
}

Calendar.defaultProps = {
  onDayPress: () => {},
};

Calendar.propTypes = {
  onDayPress: PropTypes.func,
};
