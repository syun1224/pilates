import React, { useEffect, createRef, useState } from 'react';
import {
  View, ScrollView, SafeAreaView,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import Header from '../../../components/Header';
import { Text, Input } from '../../../components/material';


export default function SurveyModal({ navigation }) {
  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <Header navigation={navigation} color="#fff" goBack>만족도 조사</Header>
      <ScrollView style={{ flex: 1 }}>
        {}
      </ScrollView>
    </View>
  );
}

SurveyModal.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
