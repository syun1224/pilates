import React, { useEffect, createRef, useState } from 'react';
import {
  View, ScrollView, SafeAreaView,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import Header from '../../components/Header';
import { Text, Input } from '../../components/material';
import { REM } from '../../../constants/Config';

const data = [
  {
    startAt: '07:00',
    EndAt: '07:50',
    week: [{ name: '스프링' }, {}, { name: '리포머' }, {}, { name: '스프링' }, {}, { name: '리포머' }],
  },
  {
    startAt: '08:00',
    EndAt: '08:50',
    week: [{ name: '리포머' }, { name: '스프링' }, {}, { name: '리포머' }, { name: '스프링' }, { name: '리포머' }, {}],
  },
  {
    startAt: '09:00',
    EndAt: '09:50',
    week: [{ name: '보수' }, { name: '리포머' }, { name: '보수' }, { name: '리포머' }, { name: '보수' }, { name: '보수' }, { name: '리포머' }],
  },
  {
    startAt: '10:00',
    EndAt: '10:50',
    week: [{ name: '보수' }, { name: '리포머' }, {}, {}, { name: 'TRX' }, { name: '보수' }, { name: '보수' }],
  },
  {
    startAt: '11:00',
    EndAt: '11:50',
    week: [{ name: '스프링' }, {}, { name: '리포머' }, {}, { name: '스프링' }, { name: '보수' }, { name: '리포머' }],
  },
  {
    startAt: '13:00',
    EndAt: '13:50',
    week: [{ name: '보수' }, { name: '리포머' }, {}, {}, { name: 'TRX' }, { name: '보수' }, { name: '보수' }],
  },
  {
    startAt: '14:00',
    EndAt: '14:50',
    week: [{ name: '보수' }, { name: '리포머' }, { name: '스프링' }, { name: '리포머' }, { name: '보수' }, { name: '리포머' }, { name: '스프링' }],
  },
  {
    startAt: '15:00',
    EndAt: '15:50',
    week: [{ name: '스프링' }, {}, { name: '리포머' }, {}, { name: '스프링' }, {}, { name: '리포머' }],
  },
  {
    startAt: '16:00',
    EndAt: '16:50',
    week: [{ name: '보수' }, { name: '리포머' }, { name: '보수' }, {}, { name: '스프링' }, { name: '보수' }, { name: '스프링' }],
  },
  {
    startAt: '17:00',
    EndAt: '17:50',
    week: [{ name: '보수' }, { name: '리포머' }, { name: '보수' }, { name: '리포머' }, { name: '리포머' }, { name: '보수' }, { name: '리포머' }],
  },
  {
    startAt: '18:00',
    EndAt: '18:50',
    week: [{ name: '보수' }, { name: '리포머' }, {}, { name: '리포머' }, { name: '스프링' }, { name: '보수' }, { name: '리포머' }],
  },
  {
    startAt: '19:00',
    EndAt: '19:50',
    week: [{ name: '스프링' }, {}, { name: '리포머' }, {}, { name: '스프링' }, { name: '리포머' }, { name: '리포머' }],
  },
  {
    startAt: '20:00',
    EndAt: '20:50',
    week: [{ name: '스프링' }, {}, { name: '스프링' }, {}, { }, { }, { name: '스프링' }],
  },
];

const styles = EStyleSheet.create({
  cellWarp: {
    width: '44.5rem',
    height: '38rem',
    borderTopWidth: '1rem',
    borderTopColor: '#5555',
    borderLeftWidth: '1rem',
    borderLeftColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  timeCellWrap: {
    width: '59rem',
    height: '38rem',
    borderTopWidth: '1rem',
    borderTopColor: '#5555',
    borderLeftWidth: '1rem',
    borderLeftColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  lastLine: {
    borderRightWidth: '1rem',
    borderRightColor: '#ffff',
  },
  underLine: {
    borderBottomWidth: '1rem',
    borderBottomColor: '#5555',
  },
  titleContainer: {
    padding: '10rem',
    borderBottomWidth: '1rem',
    borderBottomColor: '#5555',
  },
  titleTopWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconWrap: {
    width: '30rem',
    height: '20rem',
    backgroundColor: '#5555',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: '5rem',
    borderRadius: '15rem',
  },
  arrowWrap: {
    position: 'absolute', right: '5rem', top: 0, bottom: 0, justifyContent: 'center', alignItems: 'center',
  },
});

export default function TimeTable({ navigation }) {
  const dayOfWeekArr = [null, '월', '화', '수', '목', '금', '토', '일'];
  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <Header navigation={navigation} color="#fff" goBack> 5월 그룹 시간표</Header>
      <ScrollView style={{ flex: 1 }} contentContainerStyle={{ alignItems: 'center', paddingVertical: 20 * REM }}>
        <View>
          <View style={{ flexDirection: 'row', backgroundColor: '#7774' }}>
            {dayOfWeekArr.map((str, i) => {
              if (str) return <View style={[styles.cellWarp, i === dayOfWeekArr.length - 1 && styles.lastLine]}><Text>{str}</Text></View>;
              return (
                <View style={styles.timeCellWrap}>
                  <Text size={12}>시작시간/</Text>
                  <Text size={12}>종료시간</Text>
                </View>
              );
            })}
          </View>
          <View>
            {data.map((obj, idx) => (
              <View style={{ flexDirection: 'row' }}>
                <View style={[styles.timeCellWrap, idx === data.length - 1 && styles.underLine, { backgroundColor: '#7771' }]}>
                  <Text size={13}>{obj.startAt}~</Text>
                  <Text size={13}>{obj.EndAt}</Text>
                </View>
                {obj.week && obj.week.map((day, innerIdx) => {
                  let backgroundColor = '#fff';
                  if (day.name === '리포머') backgroundColor = '#5f55';
                  else if (day.name === '보수') backgroundColor = '#f555';
                  else if (day.name === '스프링') backgroundColor = '#99f5';
                  else if (day.name === 'TRX') backgroundColor = '#f4f5';
                  return (
                    <View style={[
                      styles.cellWarp,
                      innerIdx === obj.week.length - 1 && styles.lastLine,
                      idx === data.length - 1 && styles.underLine,
                      { backgroundColor },
                    ]}
                    >
                      {day.name ? <Text color="#333">{day.name}</Text> : <View />}
                    </View>
                  );
                })}
              </View>
            ))}
          </View>
        </View>
      </ScrollView>
    </View>
  );
}

TimeTable.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
