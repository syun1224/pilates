import React, { useEffect, createRef, useState } from 'react';
import {
  View, ScrollView, SafeAreaView,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import Header from '../../components/Header';
import { Text, Input } from '../../components/material';
import { REM } from '../../../constants/Config';

const styles = EStyleSheet.create({
  termsWarp: {
    marginVertical: '20rem',
    width: '100%',
    height: '300rem',
    borderWidth: '1rem',
    borderColor: '#5555',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default function Policy({ navigation }) {
  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <Header navigation={navigation} color="#fff" goBack>이용약관 / 정책</Header>
      <ScrollView style={{ flex: 1 }} contentContainerStyle={{ padding: 20 * REM }}>
        <Text size={19}>이용약관</Text>
        <View style={styles.termsWarp}>
          <ScrollView style={{ flex: 1 }} />
        </View>
        <Text size={19}>개인정보 처리방침</Text>
        <View style={styles.termsWarp}>
          <ScrollView style={{ flex: 1 }} />
        </View>
      </ScrollView>
    </View>
  );
}

Policy.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
