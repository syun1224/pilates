import React, { useEffect, createRef, useState } from 'react';
import {
  View, ScrollView, SafeAreaView, Picker,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import Header from '../../components/Header';
import {
  Text, Input, Button, CustomModal,
} from '../../components/material';
import { REM } from '../../../constants/Config';
import Selector from '../../components/Selector';

const color = '#faa';

const styles = EStyleSheet.create({
  topLine: {
    width: '100%',
    borderBottomWidth: '1rem',
    borderBottomColor: '#9996',
  },
  surveyListWrap: {
    width: '100%',
  },
  surveyWrap: {
    width: '100%',
  },
  listWrap: {
    width: '100%',
    height: '70rem',
    alignSelf: 'center',
    borderBottomWidth: '1rem',
    borderBottomColor: '#9996',
    alignItems: 'center',
    flexDirection: 'row',
  },

  totalWrap: {
    height: '40rem',
    width: '350rem',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: '40rem',
  },
  cardShadow: {
    shadowOffset: { width: 0, height: 0.5 * 3 },
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * 3,
  },
  cardWrap: {
    height: '100rem',
    width: '350rem',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '20rem',
    borderRadius: '20rem',
    paddingHorizontal: '40rem',
    backgroundColor: '#fff',
  },
  dateWrap: { width: '100%', paddingVertical: '5rem' },
  classWrap: { width: '100%', paddingVertical: '5rem' },
  countWrap: { width: '100%', alignItems: 'flex-end', paddingVertical: '5rem' },

  emptyContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyTextWrap: {
    marginBottom: '20rem',
  },
  pickerWrap: {
    height: '50rem',
    width: '350rem',
    borderColor: '#5555',
    borderWidth: '1rem',
    alignItems: 'center',
  },
  picker: {
    height: '50rem',
    width: '350rem',
  },
});


export default function Question({ navigation }) {
  const [visible, setVisible] = useState(false);
  const [branch, setBranch] = useState(false);

  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <Header navigation={navigation} color="#fff" goBack>문의사항</Header>
      <View style={styles.emptyContainer}>
        <View style={styles.emptyTextWrap}><Text size={16} color="#3339">등록된 문의가 없습니다</Text></View>
        <Button
          height={30}
          width={90}
          fontSize={15}
          border="1 20 #5550"
          backgroundColor={color}
          color="#fff"
          onPress={() => setVisible(true)}
        >
          문의하기
        </Button>
        <SafeAreaView />
      </View>
      {/* <ScrollView style={{ flex: 1 }}>
        {}
      </ScrollView> */}
      <CustomModal
        visible={visible}
        onPressBackground={() => setVisible(false)}
        animationType="none"
        leftButton={{ text: '확인', onPress: () => setVisible(false) }}
        rightButton={{ text: '취소', onPress: () => setVisible(false) }}
        height={430}
      >
        <View style={{ alignItems: 'center' }}>
          <View style={{ paddingBottom: 15 * REM }}>
            <Text size={18} weight="bold">문의하기</Text>
          </View>
          <Selector data={['강남', '신사', '성수', '종로']} />
          <View style={{ paddingTop: 10 * REM }}>
            <Input
              height={40}
              width={250}
              border="1 5 #5555"
              placeholder="제목"
              fontSize={16}
            />
          </View>
          <View style={{ paddingTop: 5 * REM }}>
            <Input
              height={200}
              width={250}
              border="1 5 #5555"
              placeholder="문의 내용"
              fontSize={16}
              multiline
            />
          </View>
        </View>
      </CustomModal>
    </View>
  );
}

Question.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
