import React, { useEffect, useRef, useState } from 'react';
import {
  View, ScrollView, SafeAreaView, Animated, FlatList, Dimensions,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';

import Header from '../../components/Header';
import { Text, Input } from '../../components/material';
import { REM } from '../../../constants/Config';

const defaultColor = '#faa';

const { width } = Dimensions.get('window');

const data = [
  {
    name: '강남점',
    phoneNumber: '02-1111-1111',
    address: '서울특별시 강남구 역삼1동 823-36',
  },
  {
    name: '서초점',
    phoneNumber: '02-2222-2222',
    address: '서울특별시 서초구 서초3동 1543-13',
  },
  {
    name: '성수점',
    phoneNumber: '02-3333-3333',
    address: '서울특별시 성동구 성수2가3동 315-62',
  },
];

const styles = EStyleSheet.create({
  listContainer: {
    width,
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: '1rem',
    borderBottomColor: defaultColor,
    marginBottom: '10rem',
  },
  titleContainer: {
    flex: 1,
    height: '100%',
    padding: '10rem',
  },
  titleTopWrap: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '5rem',
  },
  imgWrap: {
    width: '90rem',
    height: '90rem',
    backgroundColor: defaultColor,
  },
});

export default function BranchInfo({ navigation }) {
  const renderItem = (item, index) => (
    <View key={index} style={styles.listContainer}>
      <View style={styles.imgWrap} />
      <View style={styles.titleContainer}>
        <View style={styles.titleTopWrap}>
          <Text size={16}>{item.name}</Text>
          <View style={{ height: 40 * REM }} />
          <Text size={16} style={{ alignSelf: 'center' }}>{item.phoneNumber}</Text>
        </View>
        <View><Text>{item.address}</Text></View>
      </View>
    </View>
  );


  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <Header navigation={navigation} color="#fff" goBack>지점 안내</Header>
      <FlatList
        style={{ width: '100%' }}
        contentContainerStyle={{ alignItems: 'center', paddingTop: 10 * REM }}
        data={data}
        renderItem={({ item, index }) => renderItem(item, index)}
      />
    </View>
  );
}

BranchInfo.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
