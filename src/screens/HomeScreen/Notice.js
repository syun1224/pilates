import React, { useEffect, useRef, useState } from 'react';
import {
  View, ScrollView, SafeAreaView, Animated, FlatList, TouchableWithoutFeedback, LayoutAnimation,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';

import Header from '../../components/Header';
import { Text, Input } from '../../components/material';
import { REM } from '../../../constants/Config';


const defaultColor = '#faa';

const data = [
  {
    date: '2020-01-01',
    title: '안녕하세요 강남점 이용안내입니다.',
    content: '그룹, 개인, 듀엣 레슨 모두 온라인을 통해 레슨권 구매 가능하며, 포인트 적립 및 다양한 할인 혜택을 확인해보세요. 해당 지점에서 상담 및 등록 후 바로 이용 가능합니다.',
  },
  {
    date: '2020-01-02',
    title: '안녕하세요 강남점 강사 교육/채용 연계 프로그램 안내입니다.',
    content: '필라테스 전문 강사 교육과 인재채용을 위한 교육기관입니다. 상업적인 교육 프로그램이 아닌 더센터의 실력있는 강사 양성을 목표로, 지도자에게 꼭 필요한 이론 및 실습 교육 과정과 전국 33개 지점 채용 연계까지 이어지는 체계적인 커리큘럼을 제공합니다.',

  },
];

const styles = EStyleSheet.create({
  listContainer: {
    width: '100%',
    // borderWidth: '1rem',
    // borderColor: '#5555',
    overflow: 'hidden',
  },
  titleContainer: {
    padding: '10rem',
    borderBottomWidth: '1rem',
    borderBottomColor: '#faa',
  },
  titleTopWrap: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: '5rem',
  },
  iconWrap: {
    width: '35rem',
    height: '20rem',
    backgroundColor: defaultColor,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: '5rem',
    borderRadius: '15rem',
  },
  arrowWrap: {
    position: 'absolute',
    right: '5rem',
    top: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default function Notice({ navigation }) {
  const [animationArr, setAnimationArr] = useState([]);

  useEffect(() => {
    data.forEach((obj) => {
      animationArr.push(false);
    });
  }, []);

  const updateLayout = (index) => {
    setAnimationArr([...animationArr.slice(0, index), !animationArr[index], ...animationArr.slice(index + 1)]);
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
  };

  const renderItem = (item, index) => (
    <View key={index} style={styles.listContainer}>
      <TouchableWithoutFeedback onPress={() => updateLayout(index)}>
        <View style={styles.titleContainer}>
          <View style={styles.titleTopWrap}>
            <View style={styles.iconWrap}><Text color="#fff">알림</Text></View>
            <Text style={{ alignSelf: 'center' }}>{item.date}</Text>
          </View>
          <View><Text>{item.title}</Text></View>
          <View style={styles.arrowWrap}>
            <AntDesign style={{ transform: [{ rotate: animationArr[index] ? '180deg' : '0deg' }] }} size={20 * REM} name="down" />
          </View>
        </View>
      </TouchableWithoutFeedback>
      <View style={{
        backgroundColor: '#faa',
        height: animationArr[index] ? null : 0,
      }}
      >
        <View style={{ padding: 20 * REM }}><Text color="#fff">{item.content}</Text></View>
      </View>
    </View>
  );

  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <Header navigation={navigation} color="#fff" goBack>공지사항</Header>
      <FlatList
        style={{ width: '100%' }}
        contentContainerStyle={{ alignItems: 'center', paddingTop: 10 * REM }}
        data={data}
        renderItem={({ item, index }) => renderItem(item, index)}
      />
    </View>
  );
}

Notice.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
