import React, { useEffect } from 'react';
import { SafeAreaView } from 'react-native';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import Home from './Home';
import MyPage from './MyPage';
import Notice from './Notice';
import TimeTable from './TimeTable';
import Question from './Question';
import Policy from './Policy';
import Survey from './Survey';
import BranchInfo from './BranchInfo';

export default function Main(props) {
  const Stack = createStackNavigator();
  useEffect(() => {
  }, []);
  return (
    <>
      <SafeAreaView style={{ backgroundColor: '#fff' }} />
      <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Home">
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="MyPage" component={MyPage} options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }} />
        <Stack.Screen name="TimeTable" component={TimeTable} options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }} />
        <Stack.Screen name="Notice" component={Notice} options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }} />
        <Stack.Screen name="Survey" component={Survey} options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }} />
        <Stack.Screen name="Question" component={Question} options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }} />
        <Stack.Screen name="Policy" component={Policy} options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }} />
        <Stack.Screen name="BranchInfo" component={BranchInfo} options={{ cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS }} />
      </Stack.Navigator>
    </>
  );
}
