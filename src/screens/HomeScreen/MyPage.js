import React, { useEffect, createRef, useState } from 'react';
import {
  View, ScrollView, SafeAreaView,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import Header from '../../components/Header';
import { Text, Input, Button } from '../../components/material';
import { REM } from '../../../constants/Config';

const color = '#faa';

const styles = EStyleSheet.create({
  topLine: {
    width: '100%',
    borderBottomWidth: '1rem',
    borderBottomColor: '#9996',
  },
  surveyListWrap: {
    width: '100%',
  },
  surveyWrap: {
    width: '100%',
  },
  listWrap: {
    width: '100%',
    height: '70rem',
    alignSelf: 'center',
    borderBottomWidth: '1rem',
    borderBottomColor: '#9996',
    alignItems: 'center',
    flexDirection: 'row',
  },


  totalWrap: {
    height: '25rem',
    width: '350rem',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: '40rem',
  },
  cardShadow: {
    shadowOffset: { width: 0, height: 0.5 * 3 },
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * 3,
  },
  cardWrap: {
    height: '100rem',
    width: '350rem',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '20rem',
    borderRadius: '20rem',
    paddingHorizontal: '30rem',
    backgroundColor: '#fff',
  },
  dateWrap: { width: '100%', paddingVertical: '5rem', marginBottom: '5rem' },
  classWrap: { width: '100%', paddingVertical: '5rem' },
  countWrap: { width: '100%', alignItems: 'flex-end', paddingVertical: '5rem' },
});

export default function MyPage({ navigation }) {
  return (
    <View style={{ flex: 1, backgroundColor: '#fff' }}>
      <Header navigation={navigation} color="#fff" goBack>마이페이지</Header>
      <ScrollView style={{ flex: 1 }} contentContainerStyle={{ alignItems: 'center', paddingTop: 20 * REM }}>
        <View style={[styles.cardWrap, styles.cardShadow, { backgroundColor: color, height: 260 * REM }]}>
          <View style={[styles.dateWrap, { flexDirection: 'row' }]}><Text size={18} color="#fff" weight="bold">OOO님</Text></View>

          <View style={[styles.totalWrap, { marginBottom: 20 * REM }]}>
            <Text size={15} color="#fff" weight="bold">총 이용권</Text>
            <Text size={15} color="#fff" weight="bold">15/20</Text>
          </View>

          <View style={[styles.dateWrap, { flexDirection: 'row', justifyContent: 'space-between' }]}>
            <Text size={18} color="#fff" weight="bold">회원정보</Text>
            <Button width={50} height={20} backgroundColor="#fff" border="0 5" color={color}>수정</Button>
          </View>
          <View style={styles.totalWrap}>
            <Text size={15} color="#fff" weight="bold">회원 번호</Text>
            <Text size={15} color="#fff" weight="bold">1212121212</Text>
          </View>
          <View style={styles.totalWrap}>
            <Text size={15} color="#fff" weight="bold">연락처</Text>
            <Text size={15} color="#fff" weight="bold">010-0101-0101</Text>
          </View>
          <View style={styles.totalWrap}>
            <Text size={15} color="#fff" weight="bold">생년월일</Text>
            <Text size={15} color="#fff" weight="bold">1999년 01년 01일</Text>
          </View>
          <View style={styles.totalWrap}>
            <Text size={15} color="#fff" weight="bold">주소</Text>
            <Text size={15} color="#fff" weight="bold">서울시 OO구 OO동 111-11번지</Text>
          </View>
          <View style={styles.totalWrap}>
            <Text size={15} color="#fff" weight="bold">등록지점</Text>
            <Text size={15} color="#fff" weight="bold">강남점</Text>
          </View>
        </View>
        <View style={{ paddingTop: 20 * REM }}>
          <Text size={19} weight="bold">이용권 정보</Text>
        </View>
        <View style={[styles.cardWrap, styles.cardShadow]}>
          <View style={styles.dateWrap}><Text size={16}>2020-01-14 ~ 2020-06-23</Text></View>
          <View style={styles.classWrap}><Text size={16}>그룹 필라테스</Text></View>
          <View style={styles.countWrap}><Text size={16}>5/10</Text></View>
        </View>
        <View style={[styles.cardWrap, styles.cardShadow]}>
          <View style={styles.dateWrap}><Text size={16}>2020-01-14 ~ 2020-06-23</Text></View>
          <View style={styles.classWrap}><Text size={16}>개인 필라테스</Text></View>
          <View style={styles.countWrap}><Text size={16}>5/10</Text></View>
        </View>
      </ScrollView>
    </View>
  );
}

MyPage.propTypes = {
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
