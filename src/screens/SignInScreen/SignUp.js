import React, { useEffect, useState } from 'react';
import {
  View, Image, KeyboardAvoidingView, Alert, TouchableOpacity, Animated, Dimensions, ScrollView, Picker,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import { CommonActions } from '@react-navigation/native';
import EStyleSheet from 'react-native-extended-stylesheet';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import PropTypes from 'prop-types';

import { REM } from '../../../constants/Config';
import { Text, Input, Button } from '../../components/material';
import Header from '../../components/Header';

export default function SignUp(props) {
  const { navigation } = props;
  const { height, width } = Dimensions.get('window');
  const styles = EStyleSheet.create({
    tabContainer: {
      flex: 1, backgroundColor: '#aaa', flexDirection: 'column',
    },
    agreeTab: {
      flexDirection: 'row', alignItems: 'center', marginBottom: '10rem', paddingRight: '30rem', justifyContent: 'space-between',
    },
  });

  // const [left, setLeft] = useState(0)
  const left = new Animated.Value(0);

  useEffect(() => {
  }, []);

  const next = () => {
    Animated.timing(left, {
      toValue: -width,
      duration: 300,
      useNativeDriver: false,
    }).start(() => {});
  };

  return (
    <View style={{ flex: 1 }}>
      <Header navigation={navigation} goBack color="#5558" fontColor="#fff" height={50}>회원가입</Header>
      <Animated.View style={{
        flex: 1, width: width * 2, backgroundColor: '#fff', zIndex: -1, left, flexDirection: 'row',
      }}
      >

        <View style={{ flex: 1, width, backgroundColor: '#fff' }}>
          <View>
            <Text size={22}>필라테스 강남점에 오신 것을</Text>
            <Text size={22}>환영합니다!</Text>
          </View>
          <View>
            <TouchableOpacity>
              <View style={{
                flexDirection: 'row', alignItems: 'center', marginTop: 20 * REM, marginBottom: 10 * REM,
              }}
              >
                <Icon name="checkbox-blank-circle-outline" color="#999" size={27 * REM} />
                <Text size={16} style={{ marginLeft: 5 * REM }}>약관 전체동의</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView
          style={{
            flex: 1, width, backgroundColor: '#fff',
          }}
          contentContainerStyle={{ paddingTop: 30 * REM }}
        >
          <View style={{
            paddingLeft: 20 * REM, paddingBottom: 30 * REM, borderBottomColor: '#9994', borderBottomWidth: 1 * REM,
          }}
          >
            <Text size={23}>본인확인을 위해</Text>
            <Text size={23}>인증을 진행해 주세요.</Text>
            <TouchableOpacity>
              <View style={{
                flexDirection: 'row', alignItems: 'center', marginTop: 20 * REM, marginBottom: 10 * REM,
              }}
              >
                <Icon name="checkbox-blank-circle-outline" color="#999" size={27 * REM} />
                <Text size={16} style={{ marginLeft: 5 * REM }}>본인 인증 서비스 약관 전체동의</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.agreeTab}>
                <Text>휴대폰 본인 인증 서비스 약관 동의 (필수)</Text>
                <Ionicons name="ios-arrow-forward" color="#999" size={21 * REM} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.agreeTab}>
                <Text>휴대폰 통신사 이용약관 동의 (필수)</Text>
                <Ionicons name="ios-arrow-forward" color="#999" size={21 * REM} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.agreeTab}>
                <Text>개인정보 제공 및 이용 동의 (필수)</Text>
                <Ionicons name="ios-arrow-forward" color="#999" size={21 * REM} />
              </View>
            </TouchableOpacity>
            <TouchableOpacity>
              <View style={styles.agreeTab}>
                <Text>고유식별정보 처리 동의 (필수)</Text>
                <Ionicons name="ios-arrow-forward" color="#999" size={21 * REM} />
              </View>
            </TouchableOpacity>
          </View>
          <View style={{ paddingHorizontal: 20 * REM }}>
            <View style={{
              borderBottomColor: '#9994', borderBottomWidth: 1 * REM, height: 60 * REM, marginBottom: 1 * REM, paddingBottom: 1 * REM,
            }}
            >
              <Input fontSize={18} width="100%" style={{ flex: 1 }} placeholder="이름" />
            </View>
            <View style={{
              borderBottomColor: '#9994', borderBottomWidth: 1 * REM, height: 60 * REM, flexDirection: 'row', alignItems: 'center', marginBottom: 1 * REM, paddingBottom: 1 * REM,
            }}
            >
              <Input fontSize={18} width="100%" style={{ flex: 1, height: 60 * REM }} placeholder="주민등록번호" />
              <Text size={22}>-</Text>
              <Input fontSize={18} width="100%" style={{ flex: 1, height: 60 * REM }} placeholder="" />
            </View>
            <View style={{
              borderBottomColor: '#9994', borderBottomWidth: 1 * REM, height: 60 * REM, flexDirection: 'row', alignItems: 'center', marginBottom: 1 * REM, paddingBottom: 1 * REM,
            }}
            >
              <Picker style={{ height: 60 * REM, width: 100 * REM }}>
                <Picker.Item label="SKT" value="SKT" />
                <Picker.Item label="KT" value="KT" />
                <Picker.Item label="LG U+" value="LG U+" />
              </Picker>
              <Input fontSize={18} width="100%" style={{ flex: 1, height: 60 * REM }} placeholder="휴대폰 번호" />
              <TouchableOpacity>
                <View style={{
                  height: 60 * REM, width: 60 * REM, justifyContent: 'center', alignItems: 'center',
                }}
                >
                  <Text>인증요청</Text>
                </View>
              </TouchableOpacity>
            </View>
            <Text size={12} color="#5557" style={{ marginTop: 10 * REM, marginBottom: 40 * REM }}>* 타인의 개인정보를 도용하여 기입한 경우, 서비스 이용 제한 및 법적 제재를 받으실 수 있습니다.</Text>
          </View>
        </ScrollView>
      </Animated.View>
      <View style={{
        height: 55 * REM, justifyContent: 'center', alignItems: 'center',
      }}
      >
        <TouchableOpacity onPress={() => {
          next();
        }}
        >
          <View style={{
            width, height: 55 * REM, justifyContent: 'center', alignItems: 'center', backgroundColor: '#555', opacity: 0.5,
          }}
          >
            <Text color="#fff" size={20}>다음</Text>
          </View>
        </TouchableOpacity>
      </View>
      <KeyboardAvoidingView behavior="height" />
    </View>
  );
}

SignUp.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  }).isRequired,
};
