import React, { useEffect } from 'react';
import {
  View, Image, KeyboardAvoidingView, Alert, TouchableOpacity,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import { CommonActions } from '@react-navigation/native';

import PropTypes from 'prop-types';
import { REM } from '../../../constants/Config';
import { Text, Input, Button } from '../../components/material';

export default function SignIn(props) {
  const { navigation } = props;
  useEffect(() => {
  }, []);
  return (
    <>
      <Image
        style={{
          position: 'absolute', width: '100%', height: '100%', flex: 1, opacity: 0.7,
        }}
        source={require('../../../assets/images/pilates.jpg')}
      />
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
        <View
          style={{ flex: 1, alignItems: 'center', justifyContent: 'space-between' }}
        >
          <View style={{ marginTop: 100 * REM }}>
            <Text size={40 * REM} weight="bold">PILATES</Text>
          </View>

          <View style={{ marginBottom: 50 * REM }}>
            <Input
              height={40}
              width={200}
              border="1 15"
              paddingHorizontal={10}
              placeholder="아이디"
            />
            <View style={{ height: 10 * REM }} />
            <Input
              height={40}
              width={200}
              border="1 15"
              paddingHorizontal={10}
              secureTextEntry
              placeholder="비밀 번호"
            />
            <View style={{ height: 10 * REM }} />
            <Button
              height={40}
              width={200}
              backgroundColor="rgba(100,100,100, 0.6)"
              border="1 15"
              color="#fff"
              fontSize={16}
              onPress={() => {
                navigation.dispatch(
                  CommonActions.reset({
                    index: 0,
                    routes: [
                      { name: 'Home' },
                    ],
                  }),
                );
              }}
            >
              SIGN IN
            </Button>
            {/* <TouchableOpacity onPress={() => navigation.navigate('SignUp')}>
              <View style={{ height: 40 * REM, justifyContent: 'center', alignItems: 'center' }}>
                <Text weight="bold">회원가입</Text>
              </View>
            </TouchableOpacity> */}
          </View>
        </View>
      </KeyboardAvoidingView>
    </>
  );
}

SignIn.propTypes = {
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
  }).isRequired,
};
