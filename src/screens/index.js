/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */

import React, { useState, useRef, useEffect } from 'react';
import {
  StatusBar, View, Dimensions, Easing, Platform, SafeAreaView, Alert, UIManager,
} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import messaging from '@react-native-firebase/messaging';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import EStyleSheet from 'react-native-extended-stylesheet';
import { SafeAreaProvider } from 'react-native-safe-area-context';

// import useLinking from './navigation/useLinking';
import HomeScreen from './HomeScreen';
import SignInScreen from './SignInScreen';

import Layout from '../../constants/Layout';
import Colors from '../../constants/Colors';

const Stack = createStackNavigator();
const { AuthorizationStatus } = messaging;

export default function Screen(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);
  const [initialNavigationState, setInitialNavigationState] = useState();
  const containerRef = useRef();
  // const { getInitialState } = useLinking(containerRef);
  const { width } = Dimensions.get('window');

  const REM = width / 375;

  EStyleSheet.build({
    $rem: REM,
    $defaultColor: Colors.defaultColor,
  });

  async function requestUserPermission() {
    const authStatus = await messaging().requestPermission();
    const enabled = authStatus === AuthorizationStatus.AUTHORIZED
      || authStatus === AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      console.log('Authorization status:', authStatus);
    }
  }

  // Load any resources or data that we need prior to rendering the app
  useEffect(() => {
    requestUserPermission();

    messaging().onNotificationOpenedApp((remoteMessage) => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
    });

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then((remoteMessage) => {
        if (remoteMessage) {
          console.log(
            'Notification caused app to open from quit state:',
            remoteMessage.notification,
          );
        }
      });

    async function loadResourcesAndDataAsync() {
      try {
        // SplashScreen.preventAutoHide();

        // Load our initial navigation state
        // setInitialNavigationState(await getInitialState());

        // Load fonts
        // await Font.loadAsync({
        //   ...Ionicons.font,
        //   'space-mono': require('../../assets/fonts/SpaceMono-Regular.ttf'),
        // });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        // SplashScreen.hide();
      }
    }
    loadResourcesAndDataAsync();

    const unsubscribe = messaging().onMessage(async (remoteMessage) => {
      Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
    });

    return unsubscribe;
  }, []);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return null;
  }

  if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  return (
    <SafeAreaProvider>
      <View style={Layout.root}>
        <StatusBar backgroundColor="#000" barStyle={Platform.OS === 'ios' ? 'dark-content' : 'light-content'} />
        <NavigationContainer ref={containerRef} initialState={initialNavigationState}>
          <Stack.Navigator screenOptions={{ headerShown: false }}>
            <Stack.Screen name="SignIn" component={SignInScreen} options={{ animationEnabled: false }} />
            <Stack.Screen name="Home" component={HomeScreen} options={{ animationEnabled: false }} />
          </Stack.Navigator>
        </NavigationContainer>
      </View>
    </SafeAreaProvider>
  );
}
