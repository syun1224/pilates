import React, { useState, useRef } from 'react';
import {
  View, Animated, Dimensions,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import LinearGradient from 'react-native-linear-gradient';

import TabButton from './components/TabButton';
import { REM } from '../../../constants/Config';

export default function BottomTabView(props) {
  const { children } = props;
  const pageWidth = Dimensions.get('window').width;
  const [screenHeight, setScreenHeight] = useState(0);
  const [bottomHeight, setBottomHeight] = useState(0);
  const [focusPage, setFocusPage] = useState(0);

  const style = EStyleSheet.create({
    tabContainer: {
      flex: 1, backgroundColor: '#aaa', flexDirection: 'column',
    },
    tab: {
      backgroundColor: '$defaultColor',
      width: '100%',
      height: '55rem',
      flexDirection: 'row',
    },
    viewPage: {
      flexDirection: 'row',
      height: screenHeight - bottomHeight || '100%',
      width: Array.isArray(children) ? (pageWidth * children.length) : pageWidth,
    },
    viewOnePage: {
      flex: 1,
      width: pageWidth,
    },
  });

  const position = useRef(new Animated.Value(0)).current;

  const movePage = (page) => {
    setFocusPage(page);
    if (typeof page !== 'number' || focusPage === page) return;
    Animated.timing(position, {
      toValue: -1 * page * pageWidth,
      duration: 0,
    }).start();
  };

  if (Array.isArray(children)) {
    return (
      <View
        style={style.tabContainer}
        onLayout={({ nativeEvent }) => {
          const { height } = nativeEvent.layout;
          setScreenHeight(height);
        }}
      >
        <Animated.View style={[style.viewPage, { left: position }]}>
          {children.map((obj) => (
            <View style={style.viewOnePage}>
              {obj}
            </View>
          ))}
        </Animated.View>
        <View
          onLayout={({ nativeEvent }) => {
            const { height } = nativeEvent.layout;
            setBottomHeight(height);
          }}
        >
          {/* <LinearGradient style={{ height: 2 * REM, width: '100%' }} start={{ x: 0, y: 1 }} end={{ x: 0, y: 0 }} colors={['#444', '#999']} /> */}
          <View
            style={style.tab}

          >
            {children.map((obj, i) => {
              const { imgName } = obj.props;
              return (
                <TabButton
                  focused={i === focusPage}
                  onPress={() => movePage(i)}
                  imgName={imgName}
                />
              );
            })}
          </View>
        </View>
      </View>
    );
  }
  return children;
}

BottomTabView.defaultProps = {
  children: { props: { image: null } },
};

BottomTabView.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.array,
  ]),
};

export {
  TabButton,
};
