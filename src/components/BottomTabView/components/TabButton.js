import React from 'react';
import { View, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import TabBarIcon from '../../TabBarIcon';


const style = EStyleSheet.create({
  tabButton: {
    height: '55rem',
    width: '70rem',
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    height: '30rem',
    tintColor: '#fff',
  },
});

export default function TabButton(props) {
  const { imgName, onPress, focused } = props;
  return (
    <TouchableWithoutFeedback onPress={onPress}>
      <View style={style.tabButton}>
        <TabBarIcon focused={focused} name={imgName} />
      </View>
    </TouchableWithoutFeedback>
  );
}

TabButton.defaultProps = {
  imgName: 'photo',
  onPress: () => {},
  focused: false,
};

TabButton.propTypes = {
  imgName: PropTypes.string,
  onPress: PropTypes.func,
  focused: PropTypes.bool,
};
