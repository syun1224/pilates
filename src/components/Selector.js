import React, { useState, useEffect } from 'react';
import {
  View, Dimensions, ScrollView, TouchableOpacity, TouchableWithoutFeedback, Animated, Alert,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet, { child } from 'react-native-extended-stylesheet';
import Modal from 'react-native-modal';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { Text } from './material';
import ColorFormatCheck from '../utils/ColorFormatCheck';
import { REM } from '../../constants/Config';

export default function Selector(props) {
  const {
    height, width, data, onSelect,

  } = props;
  const { height: containerHeight, width: containerWidth } = Dimensions.get('window');
  const [value, setValue] = useState('지점 선택');
  const [visible, setVisible] = useState(false);

  const styles = EStyleSheet.create({
    selectWrap: {
      width: '250rem',
      height: '40rem',
      paddingLeft: '10rem',
      borderRadius: '5rem',
      zIndex: 1,
      elevation: 1,
      borderColor: '#5555',
      borderWidth: '1rem',
    },
    dropdownWrap: {
      position: 'absolute',
      top: '30rem',
      width: '248rem',
      height: '150rem',
      backgroundColor: '#fff',
      shadowOffset: { width: 0, height: 0.5 * 3 },
      shadowOpacity: 0.3,
      shadowRadius: 0.8 * 3,
    },
    arrow: {
      position: 'absolute',
      right: '10rem',
    },
    list: {
      width: '248rem',
      height: '40rem',
      paddingLeft: '10rem',
      justifyContent: 'center',
      borderBottomWidth: '1rem',
      borderBottomColor: '#5553',
    },
  });

  return (
    <View style={styles.selectWrap}>
      <TouchableWithoutFeedback onPress={() => {
        setVisible(!visible);
      }}
      >
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <Text size={15}>{value}</Text>
          <AntDesign style={styles.arrow} color="#000" size={11 * REM} name="caretdown" />
        </View>
      </TouchableWithoutFeedback>
      {visible && data
      && (
        <View style={[styles.dropdownWrap]}>
          <ScrollView style={{ flex: 1 }}>
            {data.map((str) => (
              <TouchableOpacity
                onPress={() => {
                  setValue(str);
                  setVisible(!visible);
                }}
              >
                <View style={styles.list}>
                  <Text>{str}</Text>
                </View>
              </TouchableOpacity>
            ))}
          </ScrollView>
        </View>
      )}
    </View>
  );
}

Selector.defaultProps = {
  height: 400,
  width: 300,
  onSelect: () => {},
  data: [],
};

Selector.propTypes = {
  height: PropTypes.number,
  width: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  onSelect: PropTypes.func,
  data: PropTypes.array,
};
