import React from 'react';
import {
  View, TouchableWithoutFeedback, Image,
  // Image, TouchableOpacity, ScrollView, TextInput, Alert,
  // Keyboard, SafeAreaView, RefreshControl, TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import Entypo from 'react-native-vector-icons/Entypo';
import AntDesign from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';

import ColorFormatCheck from '../utils/ColorFormatCheck';

import { Text } from './material';
import Colors from '../../constants/Colors';
import { REM } from '../../constants/Config';

export default function Header(props) {
  const {
    children, height, color, fontSize, navigation, openMenu, fontColor, goBack, hideLine,
  } = props;

  const styles = EStyleSheet.create({
    header: {
      width: '100%',
      height: `${height}rem`,
      backgroundColor: '$defaultColor',
      justifyContent: 'center',
      alignItems: 'center',
    },
    headerLine: {
      height: '2.5rem',
      width: '100%',
      position: 'absolute',
      bottom: '-2.5rem',
    },
    image: {
      position: 'absolute',
      left: '10rem',
      justifyContent: 'center',
      alignItems: 'center',
    },
    goBack: {
      position: 'absolute',
      left: '15rem',
      justifyContent: 'center',
      alignItems: 'center',
    },
  });

  return (
    <View style={[styles.header, { backgroundColor: ColorFormatCheck(color) }]}>
      <Text size={fontSize} weight="bold" color={ColorFormatCheck(fontColor)}>{ children }</Text>
      {goBack && (
        <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
          <View style={styles.goBack}>
            <AntDesign color={ColorFormatCheck(fontColor)} size={30 * REM} name="arrowleft" />
          </View>
        </TouchableWithoutFeedback>
      )}
      {openMenu && (
      <TouchableWithoutFeedback onPress={() => openMenu()}>
        <View style={styles.image}>
          <Entypo color={ColorFormatCheck(fontColor)} size={40 * REM} name="menu" />
        </View>
      </TouchableWithoutFeedback>
      )}
      {!hideLine && <LinearGradient start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} style={styles.headerLine} colors={['rgba(120,120,120,0.5)', 'rgba(120,120,120,0)']} />}
    </View>
  );
}

Header.defaultProps = {
  children: 'Header',
  height: 58,
  color: '#fff',
  fontSize: 20,
  openMenu: false,
  fontColor: Colors.defaultColor,
  goBack: false,
  hideLine: false,
};

Header.propTypes = {
  children: PropTypes.string,
  height: PropTypes.number,
  color: PropTypes.string,
  fontSize: PropTypes.number,
  openMenu: PropTypes.func,
  fontColor: PropTypes.string,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
    dispatch: PropTypes.func.isRequired,
    goBack: PropTypes.func.isRequired,
  }).isRequired,
  goBack: PropTypes.bool,
  hideLine: PropTypes.bool,
};
