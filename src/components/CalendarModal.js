import React, { useState, useEffect } from 'react';
import { TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import { Calendar, LocaleConfig } from 'react-native-calendars';
import Modal from 'react-native-modal';

// LocaleConfig.locales.kr = {
//   monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
//   monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
//   dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
//   dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
//   today: 'Aujourd\'hui',
// };
// LocaleConfig.defaultLocale = 'kr';
const dateObj = new Date();
const defaultDate = `${dateObj.getFullYear()}-${dateObj.getMonth() < 10 ? `0${dateObj.getMonth() + 1}` : dateObj.getMonth()}-${dateObj.getDate() < 10 ? `0${dateObj.getDate()}` : dateObj.getDate()}`;
export default function CalendarModal({
  visible, close, date, setDate,
}) {
  const [selectDate, setSelectDate] = useState(defaultDate);
  useEffect(() => {
    setSelectDate(date);
  }, [date]);
  return (
    <Modal isVisible={visible} onBackdropPress={() => close()}>
      {visible && (
      <Calendar
        current={selectDate}
        onDayPress={(pressDate) => {
          const { year, month, day } = pressDate;
          setDate({ year, month, day });
          close();
        }}
        markedDates={{ [defaultDate]: { selected: true, selectedColor: '#f6f' }, [selectDate]: { selected: true } }}
      />
      )}
    </Modal>
  );
}

CalendarModal.defaultProps = {
  visible: false,
  close: () => {},
  date: defaultDate,
  setDate: () => {},
};

CalendarModal.propTypes = {
  visible: PropTypes.bool,
  close: PropTypes.func,
  date: PropTypes.string,
  setDate: PropTypes.func,
};
