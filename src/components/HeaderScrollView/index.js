import React, { useState, useEffect, useRef } from 'react';
import {
  View, ScrollView, Animated,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import ColorFormatCheck from '../../utils/ColorFormatCheck';

export default function HeaderScrollView(props) {
  const {
    header, children, lineColor, lineWidth, fixHeader, nonFixHeaderHeight,
  } = props;
  const [scrollHeight, setScrollHeight] = useState(0);
  const [headerHeight, setHeaderHeight] = useState(0);

  const styles = EStyleSheet.create({
    scrollContainer: {
      flex: 1,
      // height: '100%', width: '100%',
    },
    scrollWrap: {
      width: '100%',
      height: scrollHeight,
      backgroundColor: '#fff',
    },
    otherWrap: {
      width: '100%',
      minHeight: `${scrollHeight}-${headerHeight}`,
    },
    borderTopLine: {
      borderTopColor: ColorFormatCheck(lineColor),
      borderTopWidth: `${lineWidth}rem`,
    },
    borderBottomLine: {
      borderBottomColor: ColorFormatCheck(lineColor),
      borderBottomWidth: `${lineWidth}rem`,
    },
  });

  const fixStyles = EStyleSheet.create({
    ScrollWrap: {
      width: '100%',
    },
  });

  const yOffset = new Animated.Value(0);
  const yVelocity = new Animated.Value(0);
  const headerY = new Animated.Value(0);
  // const headerY = Animated.diffClamp(scrollY, 0, headerHeight);
  // velocityY.addListener((e) => {

  // });
  const headerTranslate = yVelocity.interpolate({
    inputRange: [0, headerHeight],
    outputRange: [0, -headerHeight],
    extrapolate: 'clamp',
  });

  if (fixHeader) {
    return (
      <View style={{ flex: 1 }}>
        <ScrollView
          style={{ flex: 1, paddingTop: headerHeight }}
          scrollEventThrottle={16}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { y: yOffset }, velocity: { y: yVelocity } } }],
          )}
        >
          {children}
        </ScrollView>
        <Animated.View style={{
          position: 'absolute', width: '100%', height: headerHeight, overflow: 'hidden',
        }}
        >
          <Animated.View style={{ top: headerTranslate }}>
            <View
              style={{ position: 'absolute', width: '100%' }}
              onLayout={({ nativeEvent }) => {
                const { height } = nativeEvent.layout;
                setHeaderHeight(height);
              }}
            >
              {header}
            </View>
          </Animated.View>
          {fixHeader}
        </Animated.View>
      </View>
    );
  }
  return (
    <View
      style={styles.scrollContainer}
      onLayout={({ nativeEvent }) => {
        const { height } = nativeEvent.layout;
        setScrollHeight(height);
      }}
    >
      <ScrollView
        style={styles.scrollWrap}
      >
        <View
          style={[styles.borderTopLine, styles.borderBottomLine]}
          onLayout={({ nativeEvent }) => {
            const { height } = nativeEvent.layout;
            setHeaderHeight(height);
          }}
        >
          {header}
        </View>
        <View style={[styles.otherWrap, styles.borderBottomLine]}>
          {children}
        </View>
      </ScrollView>
    </View>
  );
}

HeaderScrollView.defaultProps = {
  children: <View />,
  header: <View />,
  lineColor: '#fff0',
  lineWidth: 0,
  fixHeader: null,
  nonFixHeaderHeight: 0,
};

HeaderScrollView.propTypes = {
  children: PropTypes.element,
  header: PropTypes.element,
  lineColor: PropTypes.string,
  lineWidth: PropTypes.number,
  fixHeader: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.bool,
  ]),
  nonFixHeaderHeight: PropTypes.number,
};
