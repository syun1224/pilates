import React, { useState, useEffect } from 'react';
import { TextInput, Image } from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import ColorFormatCheck from '../../utils/ColorFormatCheck';

export default function Input(props) {
  const {
    height, width, fontSize, color, backgroundColor, border, style, value,
    padding, paddingHorizontal, returnKeyType, placeholder, placeholderTextColor, multiline, textContentType, secureTextEntry, onChangeText,
  } = props;
  const [text, setText] = useState(value || '');
  let borderColor = 'rgba(0,0,0,0)';
  let borderWidth = 0;
  let borderRadius = 0;

  const borderStyle = border.trim().split(' ');
  if (Array.isArray(borderStyle)) {
    borderStyle.forEach((el) => {
      if (Number.isNaN(1 * el)) {
        borderColor = el;
      } else if (borderWidth === 0) borderWidth = el;
      else borderRadius = el;
    });
  }

  const styles = EStyleSheet.create({
    input: {
      height: `${height}rem`,
      width: typeof width === 'string' && !Number.isNaN(width.split('%')[0]) ? width : `${width}rem`,
      color: ColorFormatCheck(color),
      backgroundColor: ColorFormatCheck(backgroundColor),
      borderColor: ColorFormatCheck(borderColor),
      borderWidth: `${borderWidth}rem`,
      borderRadius: `${borderRadius}rem`,
      paddingVertical: `${paddingHorizontal === 0 ? padding : 0}rem`,
      paddingHorizontal: `${paddingHorizontal === 0 ? padding : paddingHorizontal}rem`,
      fontSize: `${fontSize}rem`,
      textAlignVertical: 'center',
    },
  });

  useEffect(() => {
    onChangeText(text);
  }, [text]);

  return (
    <>
      <TextInput
        returnKeyType={returnKeyType}
        style={[styles.input, style]}
        value={text}
        autoCapitalize="none"
        autoCorrect={false}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        onChangeText={(e) => setText(e)}
        multiline={multiline}
        secureTextEntry={secureTextEntry}
        textContentType={textContentType}
        underlineColorAndroid="rgba(0,0,0,0)"
      />
      <Image />
    </>
  );
}

Input.defaultProps = {
  height: 30,
  width: 100,
  fontSize: 14,
  padding: 5,
  paddingHorizontal: 0,
  color: '#000',
  backgroundColor: '#fff',
  border: '',
  style: {},
  returnKeyType: 'done',
  placeholder: '',
  placeholderTextColor: '#3336',
  textContentType: 'none',
  multiline: false,
  secureTextEntry: false,
  value: '',
  onChangeText: () => {},
};

Input.propTypes = {
  height: PropTypes.number,
  width: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),
  fontSize: PropTypes.number,
  padding: PropTypes.number,
  paddingHorizontal: PropTypes.number,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  border: PropTypes.string,
  returnKeyType: PropTypes.string,
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  textContentType: PropTypes.string,
  multiline: PropTypes.bool,
  secureTextEntry: PropTypes.bool,
  style: PropTypes.object,
  value: PropTypes.string,
  onChangeText: PropTypes.func,
};
