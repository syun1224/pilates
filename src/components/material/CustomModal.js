import React, { useState, useEffect } from 'react';
import {
  Modal, View, Dimensions, ScrollView, TouchableOpacity, TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet, { child } from 'react-native-extended-stylesheet';

import Text from './Text';
import ColorFormatCheck from '../../utils/ColorFormatCheck';
import { REM } from '../../../constants/Config';

export default function CustomModal(props) {
  const {
    height, width, visible, children, onRequestClose, animationType, scrollEnabled, leftButton, rightButton, onPressBackground,

  } = props;
  const { height: containerHeight, width: containerWidth } = Dimensions.get('window');
  const [text, setText] = useState('');

  const styles = EStyleSheet.create({
    modalContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    background: {
      position: 'absolute',
      width: containerWidth,
      height: containerHeight,
      backgroundColor: 'rgba(0,0,0,0.5)',
    },
    modalWrap: {
      height: `${height}rem`,
      width: typeof width === 'string' && !Number.isNaN(width.split('%')[0]) ? width : `${width}rem`,
      backgroundColor: '#fff',
      borderRadius: '10rem',
    },
    buttonContainer: {
      height: '50rem',
      borderBottomStartRadius: '10rem',
      borderBottomEndRadius: '10rem',
      flexDirection: 'row',
      borderTopWidth: '1rem',
      borderTopColor: '#5555',
    },
    buttonWrap: {
      flex: 1,
      height: '50rem',
    },
    button: {
      height: '100%',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
  });

  return (
    <Modal
      visible={visible}
      transparent
      onRequestClose={onRequestClose}
      animationType={animationType}
    >
      <View style={styles.modalContainer}>
        <TouchableWithoutFeedback onPress={onPressBackground}>
          <View style={styles.background} />
        </TouchableWithoutFeedback>
        <View style={styles.modalWrap}>
          <ScrollView style={styles.modalWrap} scrollEnabled={scrollEnabled} contentContainerStyle={{ paddingVertical: 20 * REM }}>
            {children}
          </ScrollView>
          <View style={styles.buttonContainer}>
            {leftButton && (
              <View style={[styles.buttonWrap, rightButton && { borderRightColor: '#5555', borderRightWidth: 1 * REM }]}>
                <TouchableOpacity onPress={leftButton.onPress}>
                  <View style={styles.button}>
                    <Text>{leftButton.text}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
            {rightButton && (
              <View style={styles.buttonWrap}>
                <TouchableOpacity onPress={rightButton.onPress}>
                  <View style={styles.button}>
                    <Text>{rightButton.text}</Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
      </View>
    </Modal>
  );
}

CustomModal.defaultProps = {
  height: 400,
  width: 300,

  visible: false,
  children: <View />,
  onRequestClose: () => {},
  animationType: 'none',
  scrollEnabled: false,
  leftButton: null,
  rightButton: null,
  onPressBackground: () => {},
};

CustomModal.propTypes = {
  height: PropTypes.number,
  width: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
  ]),

  visible: PropTypes.bool,
  children: PropTypes.element,
  onRequestClose: PropTypes.func,
  animationType: PropTypes.oneOf(['fade', 'none']),
  scrollEnabled: PropTypes.bool,
  leftButton: PropTypes.object,
  rightButton: PropTypes.object,
  onPressBackground: PropTypes.func,
};
