import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import ColorFormatCheck from '../../utils/ColorFormatCheck';

export default function DefaultText(props) {
  const {
    children, style, size, weight, color, padding, limit,
  } = props;

  const styles = EStyleSheet.create({
    text: {
      fontSize: `${size}rem`,
      fontWeight: weight,
      color: ColorFormatCheck(color),
      padding: `${padding}rem`,
    },
  });

  return (
    <Text style={[style, styles.text]}>
      {limit && (limit < children.length) ? `${children.substr(0, limit - 1)}..` : children}
    </Text>
  );
}

DefaultText.defaultProps = {
  children: 'NO TEXT',
  style: {},
  size: 14,
  weight: 'normal',
  color: '#000',
  padding: 0,
  limit: null,
};

DefaultText.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  size: PropTypes.number,
  weight: PropTypes.string,
  color: PropTypes.string,
  padding: PropTypes.number,
  // eslint-disable-next-line react/forbid-prop-types
  style: PropTypes.object,
  limit: PropTypes.number,
};
