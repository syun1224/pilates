import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';
import Text from './Text';
import ColorFormatCheck from '../../utils/ColorFormatCheck';
import { REM } from '../../../constants/Config';

export default function Button(props) {
  const {
    children, height, width, color, backgroundColor, border, onPress, fontSize, style, fontWeight,
  } = props;

  let borderColor = 'rgba(0,0,0,0)';
  let borderWidth = 0;
  let borderRadius = 0;

  const borderStyle = border.trim().split(' ');
  if (Array.isArray(borderStyle)) {
    borderStyle.forEach((el) => {
      if (Number.isNaN(1 * el)) {
        borderColor = ColorFormatCheck(el);
      } else if (borderWidth === 0) borderWidth = el;
      else borderRadius = el;
    });
  }
  const styles = EStyleSheet.create({
    button: {
      height: `${height}rem`,
      width: `${width}rem`,
      color: ColorFormatCheck(color),
      backgroundColor: ColorFormatCheck(backgroundColor),
      borderColor: ColorFormatCheck(borderColor),
      borderWidth: `${borderWidth}rem`,
      borderRadius: `${borderRadius}rem`,
      justifyContent: 'center',
      alignItems: 'center',
    },
  });
  return (
    <TouchableOpacity onPress={onPress}>
      <View style={[styles.button, style]}>
        <Text weigh={fontWeight} color={color} size={fontSize * REM}>{children}</Text>
      </View>
    </TouchableOpacity>
  );
}

Button.defaultProps = {
  children: 'NO TEXT',
  height: 40 * REM,
  width: 40 * REM,
  fontSize: 13 * REM,
  color: '#000',
  backgroundColor: 'rgba(0,0,0,0)',
  border: '',
  onPress: () => {},
  fontWeight: 'normal',
  style: {},
};

Button.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  height: PropTypes.number,
  width: PropTypes.number,
  fontSize: PropTypes.number,
  color: PropTypes.string,
  backgroundColor: PropTypes.string,
  border: PropTypes.string,
  onPress: PropTypes.func,
  // eslint-disable-next-line react/forbid-prop-types
  fontWeight: PropTypes.string,
  style: PropTypes.object,
};
