import React from 'react';
import {
  Image,
  PanResponder,
  Animated,
  Dimensions,
  TouchableWithoutFeedback,
} from 'react-native';
import PropTypes from 'prop-types';
import EStyleSheet from 'react-native-extended-stylesheet';

export default function Barcode() {
  const styles = EStyleSheet.create({
    barcodeContainer: {
      width: null,
      height: null,
      position: 'absolute',
      alignSelf: 'center',
      bottom: '-145rem',
    },
    barcodeWrap: {
      marginTop: '15rem',
      width: '200rem',
      height: '150rem',
      borderRadius: '10rem',
      backgroundColor: '#9999',
      // justifyContent: 'center',
      paddingTop: '10rem',
      alignItems: 'center',
    },
    barcode: {
      width: '190rem',
      height: '100rem',
      backgroundColor: '#fff',
    },
  });

  // const [visible, setVisible] = useState(false);
  const { height, width } = Dimensions.get('window');
  const REM = width / 375;
  const visible = new Animated.Value(0);
  const opacity = new Animated.Value(0);
  const dynamic = new Animated.Value(1);
  const valueXY = new Animated.ValueXY();
  const value = { x: 0, y: 0 };
  valueXY.addListener((e) => {
    if (e.y >= -300 * REM) {
      opacity.setValue(0.5 * (e.y / (-300 * REM)));
      dynamic.setValue(1 + 0.7 * (e.y / (-300 * REM)));
    }
    value.y = e.y;
  });

  const panResponder = PanResponder.create({
    onMoveShouldSetPanResponder: () => true,
    onMoveShouldSetPanResponderCapture: () => true,
    onPanResponderGrant: () => {
      visible.setValue(height);
      valueXY.setOffset({ x: value.x, y: value.y });
      valueXY.setValue({ x: 0, y: 0 });
    },
    onPanResponderMove: Animated.event([null, {
      dx: valueXY.x,
      dy: valueXY.y,
    }]),
    onPanResponderRelease: () => {
      valueXY.flattenOffset();
      if (value.y > -150 * REM) {
        visible.setValue(0);
        Animated.timing(valueXY.y, {
          toValue: 0,
          duration: 200,
        }).start();
      } else {
        Animated.timing(valueXY.y, {
          toValue: -300 * REM,
          duration: 200,
        }).start();
      }
    },
  });

  const positionTranslate = () => ({
    transform: [
      { translateY: valueXY.y },
      { scale: dynamic },
    ],
  });

  const downBarcode = () => {
    valueXY.flattenOffset();
    visible.setValue(0);
    Animated.timing(valueXY.y, {
      toValue: 0,
      duration: 200,
    }).start();
  };

  return (
    <>
      <TouchableWithoutFeedback onPress={() => downBarcode()}>
        <Animated.View
          style={{
            position: 'absolute', width: '100%', height: visible, backgroundColor: 'rgb(0,0,0)', opacity,
          }}
        />
      </TouchableWithoutFeedback>
      <Animated.View
        style={[styles.barcodeContainer, positionTranslate()]}
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...panResponder.panHandlers}
      >
        <Animated.View style={[styles.barcodeWrap]}>
          <Image style={[styles.barcode]} />
        </Animated.View>
      </Animated.View>
    </>
  );
}
