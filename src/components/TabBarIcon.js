import * as React from 'react';
import Entypo from 'react-native-vector-icons/Entypo';
import PropTypes from 'prop-types';

import Colors from '../../constants/Colors';
import { REM } from '../../constants/Config';

export default function TabBarIcon({ name, focused }) {
  return (
    <Entypo
      name={name}
      size={30 * REM}
      style={{ marginBottom: -3 }}
      color={focused ? Colors.defaultColor : Colors.tabIconDefault}
    />
  );
}

TabBarIcon.defaultProps = {
  name: '',
  focused: false,
};

TabBarIcon.propTypes = {
  name: PropTypes.string,
  focused: PropTypes.bool,
};
