
import React from 'react';
import { Provider } from 'react-redux';

import Screens from './src/screens';
import configureStore from './src/store';
import rootSaga from './src/store/saga';

const store = configureStore();
store.runSaga(rootSaga);
console.disableYellowBox = true;
function App() {
  return (
    <Provider store={store}>
      <Screens />
    </Provider>
  );
}

export default App;
