import EStyleSheet from 'react-native-extended-stylesheet';

const Layout = EStyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#FFF',
  },
});
export default Layout;
