/* eslint-disable import/prefer-default-export */
import {
  Dimensions,
} from 'react-native';

const { height, width } = Dimensions.get('window');
export const REM = width / 375;
