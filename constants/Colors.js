const tintColor = '#fff';
const defaultColor = 'rgb(68,67,100)';

export default {
  defaultColor,
  defaultText: '#fff',
  tintColor,
  tabIconDefault: 'rgb(210,210,210)',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
